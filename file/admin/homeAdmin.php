<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(isset($_SESSION["user"]) && unserialize($_SESSION["user"])->haPrivilegio("A"))
		$user = unserialize($_SESSION["user"]);
	else
		header("Refresh: 3; url= " . _ROOT_DIR_ . "/");
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Homepage Admin</title>
		<style type="text/css">
			.myrow {
				margin: 20px 5px !important;
				border: 2px solid gray;
				padding: 10px 5px;
				border-radius: 0.4em;

				transition: color .15s ease-in-out,
				background-color .15s ease-in-out,
				border-color .15s ease-in-out,
				box-shadow .15s ease-in-out;
			}

			.myrow:hover{
				border-color: #17a2b8;
				background-color: #17a2b8 !important;
			}

			a > * {
				color: rgb(255, 255, 255);
				text-decoration: none;
			}

			a:hover {
				text-decoration: none;
			}

			a:visited {
				color: rgb(255, 255, 255);
				text-decoration: none;
			}

			img {
				width: 75px;
			}
		</style>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="admin";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<?php
					if(!isset($user)){
						echo "<h2>DEVI AVERE EFFETTUATO IL LOGIN ED ESSERE UN AMINISTRATORE PER ACCEDERE A QUESTA PAGINA!</h2></main>";
						include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
						die("</div></body></html>");
					}
				?>

				<div class="card-body">
					<h1 align="center">Pagina principale di amministrazione</h1>
					<br/>
					<!--Gestisci Eventi-->
					<a href="eventiAdmin.php">
						<div class="media bg-secondary myrow">
							<img src="<?php echo _ROOT_DIR_ . "/file/struct/immagini/homeAdmin/gestisciEventi.png"; ?>" alt="Gestisci Eventi" class="mr-3 mt-3">
							<div class="media-body align-self-md-center">
								<h4>Gestisci Eventi</h4>
								<p>Visualizza e modifica tutti gli eventi creati</p>
							</div>
						</div>
					</a>
					<!--Aggiungi Evento-->
					<a href="aggiungiEvento.php">
						<div class="media bg-secondary myrow">
							<img src="<?php echo _ROOT_DIR_ . "/file/struct/immagini/homeAdmin/aggiungiEvento.png"; ?>" alt="Aggiungi Evento" class="mr-3 mt-3">
							<div class="media-body align-self-md-center">
								<h4>Aggiungi Evento</h4>
								<p>Aggiungi un evento al database</p>
							</div>
						</div>
					</a>
					<!-- Statistiche-->
					<a href="statistiche.php">
						<div class="media bg-secondary myrow">
							<img src="<?php echo _ROOT_DIR_ . "/file/struct/immagini/homeAdmin/stats.png"; ?>" alt="Statistiche" class="mr-3 mt-3">
							<div class="media-body align-self-md-center">
								<h4>Statistiche</h4>
								<p>Statistiche sulle vendite</p>
							</div>
						</div>
					</a>
					<!--Richieste-->
					<a href="richieste.php">
						<div class="media bg-secondary myrow">
							<img src="<?php echo _ROOT_DIR_ . "/file/struct/immagini/homeAdmin/richieste.png"; ?>" alt="Richieste" class="mr-3 mt-3">
							<div class="media-body align-self-md-center">
								<h4>Elenco Richieste</h4>
								<p>Elenco delle richieste fatte dagli utenti</p>
							</div>
						</div>
					</a>
				</div>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
		</div>
	</body>
</html>
