<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(isset($_SESSION["user"]) && unserialize($_SESSION["user"])->haPrivilegio("A"))
		$user = unserialize($_SESSION["user"]);
	else
		header("Refresh: 3; url= " . _ROOT_DIR_ . "/");
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Richieste</title>
		<style>
			table, tr, th, td {
				margin-left: auto;
				margin-right: auto;

				vertical-align: middle !important;
				text-align: center;
				padding: 3px 3px;
				color: white;
			}
		</style>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="admin";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<?php
					if(!isset($user)){
						echo "<h2>DEVI AVERE EFFETTUATO IL LOGIN ED ESSERE UN AMINISTRATORE PER ACCEDERE A QUESTA PAGINA!</h2></main>";
						include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
						die("</div></body></html>");
					}
				?>
				<h1>Richieste Ricevute</h1>
				<div class="clearfix">
					<a class="btn btn-danger float-left" href="homeAdmin.php">Torna indietro</a>
				</div>
				<div class="dropdown">
					<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
						<?php
							switch(@$_GET["filtro"])
							{
								case "l":
									echo "Lette";
									break;
								case "nl":
									echo "Non Lette";
									break;
								default:
									echo "Tutte";
									break;
							}
						?>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="richieste.php?filtro=t">Tutte</a>
						<a class="dropdown-item" href="richieste.php?filtro=l">Lette</a>
						<a class="dropdown-item" href="richieste.php?filtro=nl">Non lette</a>
					</div>
				</div>
				<br/>
				<?php
					$db = new DB();
					if(isset($_GET["filtro"]))
					{
						if($_GET["filtro"] == "l")
							$query = "SELECT codice, email, oggetto, messaggio, cognome_nome, letto FROM richieste NATURAL JOIN email NATURAL LEFT JOIN utenti WHERE letto=1 ORDER BY codice ";
						else if($_GET["filtro"] == "nl")
							$query = "SELECT codice, email, oggetto, messaggio, cognome_nome, letto FROM richieste NATURAL JOIN email NATURAL LEFT JOIN utenti WHERE letto=0 ORDER BY codice";
					}
					if(!isset($query) || empty($query))
						$query = "SELECT codice, email, oggetto, messaggio, cognome_nome, letto FROM richieste NATURAL JOIN email NATURAL LEFT JOIN utenti ORDER BY codice";

					$res = $db->runQuery($query);
					if($res !== false && $res->num_rows > 0)
					{
						echo "<div class='table-responsive'>";
							echo "<table class='table table-dark table-bordered table-striped'>";
								echo "<thead>";
									echo "<tr>";
										echo "<th>Email</th>";
										echo "<th>Oggetto</th>";
										echo "<th>Messaggio</th>";
										echo "<th>Utente</th>";
										echo "<th>Letto</th>";
									echo "</tr>";
								echo "</thead>";
								echo "<tbody>";
									while($row = $res->fetch_assoc())
									{
										echo "<tr>";
											echo "<td>$row[email]</td>";
											switch ($row["oggetto"]) {
												case 1:
													echo "<td>Richiesta informazioni</td>";
													break;
												case 2:
													echo "<td>Reso/Rimborso</td>";
													break;
												case 3:
													echo "<td>Reclami/consigli</td>";
													break;
												default:
													echo "<td>Altro</td>";
													break;
											}
											echo "<td><button style='color: white;text-decoration: underline;' type='submit' class='btn btn-link' data-toggle='modal' data-target='#modal'>Messaggio</button><input type='hidden' value='$row[codice]'/><input type='hidden' value=\"$row[messaggio]\"/></td>";
											echo "<td>" . (is_null($row["cognome_nome"]) ? "Non registrato" : $row["cognome_nome"]) . "</td>";
											echo "<td>" . ($row["letto"] ? "✔" : "❌"). "</td>";
										echo "</tr>";
									}
								echo "<tbody>";
							echo "</table>";
						echo "</div>";
					}
					else
						echo "<h3>Non c'è nessuna richiesta al momento</h3>";

					$db->closeConnection();
				?>
				<div class="modal fade text-dark" id="modal">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- Modal Header -->
							<div class="modal-header">
								<h4 class="modal-title">Richiesta</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<!-- Modal body -->
							<div class="modal-body">
								<h4>Testo del messaggio:</h4>
								<p id="msg_text"></p>
							</div>
							<!-- Modal footer -->
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Chiudi</button>
								<button id="buttonLetto" type="button" class="btn btn-success" data-dismiss="modal">Letto</button>
								<button id="buttonCancella" type="button" class="btn btn-warning"  data-dismiss="modal">Elimina</button>
							</div>
						</div>
					</div>
				</div>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
					var id = -1
					$("tbody > tr > td > button").click(function(){
						$("#msg_text").text($(this).next().next().val())
						id = $(this).next().val()
					})

					$('#buttonLetto').click(function(){
						if(id != -1)
						{
							$.ajax({
								type: 'POST',
								url: "ajax/r_leggiRichiesta.php",
								data: { codice: id },
								dataType: "html",
								cache: false,
								complete: function() {
									id = -1
									location.reload()
								},
								error: function() {
									console.log("Errore")
									alert("La richiesta non è andata a buon fine, riprovare")
								}
							});
						}
					});

					$('#buttonCancella').click(function(){
						if(id != -1)
						{
							$.ajax({
								type: 'POST',
								url: "ajax/r_eliminaRichiesta.php",
								data: { codice: id },
								dataType: "html",
								cache: false,
								complete: function () {
									id = -1
									location.reload()
								},
								error: function() {
									console.log("Errore")
									alert("La richiesta non è andata a buon fine, riprovare")
								}
							});
						}
					});
				})
			</script>
		</div>
	</body>
</html>
