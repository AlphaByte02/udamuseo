<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(isset($_SESSION["user"]) && unserialize($_SESSION["user"])->haPrivilegio("A"))
		$user = unserialize($_SESSION["user"]);
	else
		header("Refresh: 3; url= " . _ROOT_DIR_ . "/");
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Aggiungi Evento</title>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="admin";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<?php
					if(!isset($user)){
						echo "<h2>DEVI AVERE EFFETTUATO IL LOGIN ED ESSERE UN AMINISTRATORE PER ACCEDERE A QUESTA PAGINA!</h2></main>";
						include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
						die("</div></body></html>");
					}
				?>
				<h1>Aggiungi Evento</h1>
				<h4>(Lasciare Data Inizio e Data Fine vuote se si vuole aggiungere una Visita)</h4>
				<form id="form_aggiunta" class="form" method="POST" autocomplete="off">
					<div class="form-group">
						<label for="titolo">Titolo<span class="obbligatorio">*</span>:</label>
						<input type="text" class="form-control" name="titolo" id="titolo" placeholder="Titolo" required>
					</div>
					<div class="form-group">
						<label for="desc">Descrizione:</label>
						<input type="text" class="form-control" name="descrizione" id="descrizione" placeholder="Descrizione..." >
					</div>
					<div class="form-group">
						<label for="tariffa">Tariffa (in €)<span class="obbligatorio">*</span>:</label>
						<input type="number" class="form-control" name="tariffa" id="tariffa" placeholder="0.0" min="0" step="0.01" required>
					</div>
					<div class="form-group">
						<label for="data_inizio">Data Inizio:</label>
						<input type="date" class="form-control" name="data_inizio" id="data_inizio">
					</div>
					<div class="form-group">
						<label for="data_fine">Data Fine:</label>
						<input type="date" class="form-control" name="data_fine" id="data_fine">
					</div>
					<div class="clearfix">
						<a class="btn btn-danger btn-md float-left" href="homeAdmin.php">Annulla</a>
						<button type="submit" class="btn btn-success btn-md float-right">Aggiungi</button>
					</div>
				</form>
			</main>
			<?php
				include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
				include "modal.php";
			?>
		</div>

		<script>
			$("#form_aggiunta").submit(function(e) {
				e.preventDefault()
				let serializeData = $(this).serialize()

				$.ajax({
					type: 'POST',
					url: <?php echo "'"._ROOT_DIR_."/file/admin/ajax/r_aggiungiEvento.php'"?>,
					data: serializeData,
					dataType: "html",
					cache: false,
					complete: function(r, ts)
					{
						if(r.responseText == "" && ts === "success")
						{
							$('#modalSuccess').modal('show');
							setTimeout(function(){
								location.href = 'eventiAdmin.php';
							}, 1500);
						}
						else
							$('#modalError').modal('show');
					},
					error: function() {
						console.log("Errore")
						alert("La richiesta non è andata a buon fine, riprovare")
					}
				});
			})
		</script>
	</body>
</html>
