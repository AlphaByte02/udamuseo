<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(isset($_SESSION["user"]) && unserialize($_SESSION["user"])->haPrivilegio("A"))
		$user = unserialize($_SESSION["user"]);
	else
		header("Refresh: 3; url= " . _ROOT_DIR_ . "/");
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php 	require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php";?>
		<title>Eventi Admin</title>

		<style>
			table, tr, th, td {
				margin-left: auto;
				margin-right: auto;

				vertical-align: middle !important;
				text-align: center;
				padding: 3px 3px;
				color: white;
			}

			.datefilter {
				display: inline;
			}
			input.datefilter {
				width: auto !important;
			}

			@media only screen and (max-width: 1280px) {
				.datefilter {
					display: block;
					margin: 10px auto;
				}
				input.datefilter {
					width: 75% !important;
				}
			}
		</style>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="admin";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<?php
					if(!isset($user)){
						echo "<h2>DEVI AVERE EFFETTUATO IL LOGIN ED ESSERE UN AMINISTRATORE PER ACCEDERE A QUESTA PAGINA!</h2></main>";
						include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
						die("</div></body></html>");
					}
				?>
				<h1 align="center">Gestione Eventi</h1>
				<div class="clearfix">
					<a class="btn btn-danger float-left" href="homeAdmin.php">Torna indietro</a>
					<a class="btn btn-success float-right" href="aggiungiEvento.php">Nuovo evento</a>
				</div>
				<br/>
				<div class="clearfix">
					<div style="width: 84%; margin-left: auto; margin-right: auto;">
						<input class='form-control' id='searchArea' type='text' placeholder='Search..'/>
						<br/>
						<form method="GET" class="form container" autocomplete="off">
							<p class="datefilter">Seleziona solo eventi che iniziano tra</p>
							<input type="date" class="form-control datefilter" name="dif" value="<?php echo isset($_GET["dif"]) ? $_GET["dif"] : "" ?>" required/>
							<p class="datefilter">e il </p>
							<input type="date" class="form-control datefilter" name="dff" value="<?php echo isset($_GET["dff"]) ? $_GET["dff"] : "" ?>" required/>
							<button type="submit" class="btn btn-primary m-2">Cerca</button>
							<button type="reset" class="btn btn-primary m-2" onclick="location.href = 'eventiAdmin.php'">Reset</button>
						</form>
						<br/>
					</div>
				</div>
				<?php
					$db = new DB();

					$query = "SELECT *, (SELECT COUNT(*) FROM biglietti WHERE codice_visita = codice GROUP BY codice_visita) AS 'nBiglietti' FROM visite";
					if(isset($_GET["dif"], $_GET["dff"]) && !empty($_GET["dif"]) && !empty($_GET["dff"]))
						$query .= " WHERE data_inizio BETWEEN '" . $db->realEscapeString($_GET["dif"]) . "' AND '" . $db->realEscapeString($_GET["dff"]) . "'";
					$query .= " ORDER BY codice DESC";

					$res = $db->runQuery($query);
					if($res !== false && $res->num_rows > 0)
					{
						echo "<div class='table-responsive'>";
							echo "<table class='table table-dark table-bordered table-striped' id='tableEventi'>";
								echo "<thead>";
									echo "<tr>";
										echo "<th>Codice</th>";
										echo "<th>Titolo</th>";
										echo "<th>Descrizione</th>";
										echo "<th>Tariffa (€)</th>";
										echo "<th>Data Inizio</th>";
										echo "<th>Data Fine</th>";
										echo "<th>Azioni</th>";
									echo "</tr>";
								echo "</thead>";
								echo "<tbody>";
									while($row = $res->fetch_assoc())
									{
										echo "<tr>";
											echo "<td id='codice_$row[codice]'>$row[codice]</td>";
											echo "<td id='titolo_$row[codice]'>$row[titolo]</td>";
											echo "<td id='titolo_$row[codice]'>" . (is_null($row["descrizione"]) ? "<i>NULL</i>" : $row["descrizione"]) . "</td>";
											echo "<td id='tariffa_$row[codice]'>$row[tariffa]</td>";
											echo "<td id='data_inizio_$row[codice]'>" . (is_null($row["data_inizio"]) ? "<i>NULL</i>" : date("d/m/Y", strtotime($row["data_inizio"]))) . "</td>";
											echo "<td id='data_fine_$row[codice]'>" . (is_null($row["data_fine"]) ? "<i>NULL</i>" : date("d/m/Y", strtotime($row["data_fine"]))) . "</td>";
											echo "<td>";
												echo "<a class='btn text-primary' data-toggle='modal' data-target='#modalModifica'><img src='"._ROOT_DIR_."/file/struct/immagini/homeAdmin/matita.png' style='width:30px;'>Modifica</a><br/>";
												if(is_null($row["nBiglietti"]))
													echo "<a class='btn text-danger' data-toggle='modal' data-target='#modal_elimina'><img src='"._ROOT_DIR_."/file/struct/immagini/homeAdmin/elimina.png' style='width:30px;'>Cancella</a>";
											echo "</td>";
										echo "</tr>";
									}
								echo "<tbody>";
							echo "</table>";
						echo "</div>";
					}
					else
						echo "<h3 align='center'>Nessun elemento trovato compreso tra queste date</h3>";
					$db->closeConnection();
				?>
			</main>

			<!-- Modal Modifica -->
			<div class="modal fade" id="modalModifica" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Modifica Evento</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<form id="form_modifica" class="form container-fluid" method="POST" autocomplete="off">
								<div class="form-group">
									<label for="codice">Numero Evento:</label>
									<input type="text" class="form-control disabled" name="codice" id="codice" placeholder="Titolo" readonly>
								</div>
								<div class="form-group">
									<label for="titolo">Titolo<span class="obbligatorio">*</span>:</label>
									<input type="text" class="form-control" name="titolo" id="titolo" placeholder="Titolo" required>
								</div>
								<div class="form-group">
									<label for="descrizione">Descrizione<span class="obbligatorio">*</span>:</label>
									<input type="text" class="form-control" name="descrizione" id="descrizione" placeholder="Descrizione">
								</div>
								<div class="form-group">
									<label for="tariffa">Tariffa (in €)<span class="obbligatorio">*</span>:</label>
									<input type="number" class="form-control" name="tariffa" id="tariffa" placeholder="0.0" min="0" step="0.01" required>
								</div>
								<div class="form-group">
									<label for="data_inizio">Data Inizio:</label>
									<input type="date" class="form-control" name="data_inizio" id="data_inizio">
								</div>
								<div class="form-group">
									<label for="data_fine">Data Fine:</label>
									<input type="date" class="form-control" name="data_fine" id="data_fine">
								</div>
								<button type="submit" id="button_modifica" class="btn btn-success btn-block">Modifica</button>
							</form>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal Elimina-->
			<div class="modal fade" id="modal_elimina" data-keyboard="false" data-backdrop="static">
				<div class="modal-dialog modal-dialog-centered">
					<div class="modal-content">
						<!-- Modal Header -->
						<div class="modal-header">
							<div class="media">
								<img <?php echo "src='"._ROOT_DIR_."/file/struct/immagini/homeAdmin/elimina.png'" ?> class="mr-3" style="width:45px;"/>
								<div class="media-body">
									<h4 class="modal-title">Conferma elimina</h4>
								</div>
							  </div>
						</div>
						<!-- Modal body -->
						<div class="modal-body" align="center">
							<h5>Sicuro di voler cancellare l'evento n°<span id="modal_idEvento">-</span>?</h5>
							<h6 class="text-warning"><strong>ATTENZIONE!</strong> QUESTA AZIONE È IRREVERSIBILE!</h6>
						</div>
						<!-- Modal Footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Annulla</button>
							<button type="button" class="btn btn-success" data-dismiss="modal" id="confermaCancella">Conferma</button>
						</div>
					</div>
				</div>
			</div>
			<?php
				include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
				include "modal.php";
			?>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){

				//Modifica evento
				$("tbody > tr > td > a.text-primary").click(function(){
					let inputs = $("#form_modifica").find("input")
					let tds = $(this).parent().siblings()
					for(let i = 0; i < inputs.length; i++)
					{
						if(i==4 || i==5)
							inputs[i].value = tds[i].innerHTML != "<i>NULL</i>" ? tds[i].innerHTML.split("/").reverse().join("-") : ""
						else
							inputs[i].value = tds[i].innerHTML != "<i>NULL</i>" ? tds[i].innerHTML : ""
					}
				})

				//Elimina evento
				var idElimina = -1
				$("tbody > tr > td > a.text-danger").click(function(){
					idElimina = $(this).parent().siblings().first().text()
					$("#modal_idEvento").text(idElimina);
				})

				$('#confermaCancella').click(function(){
					if(idElimina != -1)
					{
						$.ajax({
							type: 'POST',
							url: <?php echo "'"._ROOT_DIR_."/file/admin/ajax/r_eliminaEvento.php'"?>,
							data: { codice: idElimina },
							dataType: "html",
							cache: false,
							complete: function(r, ts){
								if(r.responseText == "" && ts === "success")
									$('#modalSuccess').modal('show')
								else
									$('#modalError').modal('show')
								setTimeout(function(){
									idElimina = -1
									location.href = location.pathname
								}, 1500);
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						});
					}
				});

				$("#form_modifica").submit(function(e){
					e.preventDefault()
					let serializeData = $(this).serialize()

					$.ajax({
						type: 'POST',
						url: <?php echo "'"._ROOT_DIR_."/file/admin/ajax/r_modificaEvento.php'"?>,
						data: serializeData,
						dataType: "html",
						cache: false,
						complete: function(r, ts){
							if(r.responseText == "" && ts === "success")
								$('#modalSuccess').modal('show')
							else
								$('#modalError').modal('show')
							setTimeout(function(){
								location.href = location.pathname
							}, 1500);
						},
						error: function() {
							console.log("Errore")
							alert("La richiesta non è andata a buon fine, riprovare")
						}
					});
				})

				if($("#searchArea").val() != "")
					search($("#searchArea").val().toLowerCase())

				$("#searchArea").on("keyup", function() {
					search($(this).val().toLowerCase())
				});

				function search(value)
				{
					$("#tableEventi > tbody tr").each(function() {
						var id = $(this).find("td:nth-child(-n+4)").text().toLowerCase()
						$(this).toggle(id.indexOf(value) !== -1)
					});
				}
			})
		</script>
	</body>
</html>
