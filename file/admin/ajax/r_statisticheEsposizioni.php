<?php
	require_once "../../struct/include/functions.php";

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$db = new DB();

		$date = $db->realEscapeString(@$_POST["date"]) ?? NULL;
		$idEsp = $db->realEscapeString(@$_POST["idEsp"]) ?? NULL;

		if(!empty($date))
		{
			try
			{
				$date = new DateTime($date);

				$res = $db->runQuery("Select codice,titolo FROM visite where MONTH(data_inizio) = {$date->format("m")} AND YEAR(data_inizio) = {$date->format("Y")}");

				if($res !== false && $res->num_rows > 0)
				{
					$opt = "<option selected value='-1'>-Seleziona un'esposizione-</option>";
					while ($el = $res->fetch_assoc())
					{
						$opt .= "<option value='$el[codice]'>$el[titolo]</option>";
					}
					echo $opt;
				}
				else
					echo "norecord";
			}
			catch(\Exception $e)
			{
				echo "error";
			}
		}
		elseif(!empty($idEsp))
		{
			$res = StatGrafico::ricavatoEsposizione($idEsp);

			if($res === false)
				echo "//error";
			else
				echo $res;
		}
		else
			echo "//error";

		$db->closeConnection();
	}
?>
