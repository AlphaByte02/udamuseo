<?php
	require_once "../../struct/include/functions.php";

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$dif = $_POST["dif"] ?? "";
		$dff = $_POST["dff"] ?? "";

		$filtro = $_POST["filtro"] ?? "t";

		$res = StatGrafico::ricavatoTotale($dif, $dff, $filtro);

		if($res === false)
			echo "//error";
		else
			echo $res;
	}
?>
