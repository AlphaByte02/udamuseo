<?php
	require_once "../../struct/include/functions.php";

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		if(isset($_POST["codice"]) && !empty($_POST["codice"]))
		{
			$db = new DB();

			$db->runQuery("DELETE FROM visite WHERE codice=" . $db->realEscapeString(trim($_POST["codice"])));

			if($db->getLastError())
				echo "Esistono biglietti associati a questa visita, impossibile cancellare";

			$db->closeConnection();
		}
	}
?>
