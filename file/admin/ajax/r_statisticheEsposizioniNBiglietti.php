<?php
	require_once "../../struct/include/functions.php";

	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["idEsp"]))
	{
		$db = new DB();

		$idEsp = $db->realEscapeString($_POST["idEsp"]);

		$res = $db->runQuery("SELECT COUNT(id_biglietto) AS 'numero_biglietti' FROM biglietti WHERE codice_visita=$idEsp");

		if($res !== false)
			echo $res->fetch_assoc()["numero_biglietti"];
		else
			echo "error";

		$db->closeConnection();
	}
?>
