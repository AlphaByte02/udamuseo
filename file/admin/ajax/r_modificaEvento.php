<?php
	require_once "../../struct/include/functions.php";

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$db = new DB();

		$codice = $db->realEscapeString(trim(@$_POST["codice"])) ?? NULL;

		$titolo = $db->realEscapeString(trim(@$_POST["titolo"])) ?? NULL;

		$descrizione = $db->realEscapeString(trim(@$_POST["descrizione"])) ?? NULL;
		if(empty($descrizione))
			$descrizione = NULL;

		$tariffa = $db->realEscapeString(trim(@$_POST["tariffa"])) ?? NULL;

		$data_inizio = $db->realEscapeString(trim(@$_POST["data_inizio"])) ?? NULL;
		if(empty($data_inizio))
			$data_inizio = NULL;
		$data_fine = $db->realEscapeString(trim(@$_POST["data_fine"])) ?? NULL;
		if(empty($data_fine))
			$data_fine = NULL;

		if(!empty($codice) && !empty($titolo) && (!empty($tariffa) || $tariffa === "0"))
		{
			if($db->runQuery("UPDATE visite SET titolo='$titolo',descrizione=" . (is_null($descrizione) ? "NULL" : "'$descrizione'") . ",tariffa=$tariffa,data_inizio=" . (is_null($data_inizio) ? "NULL" : "'$data_inizio'") . ",data_fine=" . (is_null($data_fine) ? "NULL" : "'$data_fine'") . " WHERE codice=$codice") === false)
				echo $db->getLastError();

			$db->closeConnection();
		}
		else
			echo "Errore";
	}

?>
