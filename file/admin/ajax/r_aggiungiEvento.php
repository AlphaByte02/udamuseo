<?php
	require_once "../../struct/include/functions.php";

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$db = new DB();

		$titolo = $db->realEscapeString(trim(@$_POST["titolo"])) ?? NULL;

		$descrizione = $db->realEscapeString(trim(@$_POST["descrizione"])) ?? NULL;
		if(empty($descrizione))
			$descrizione = NULL;

		$tariffa = $db->realEscapeString(trim(@$_POST["tariffa"])) ?? NULL;

		$data_inizio = $db->realEscapeString(trim(@$_POST["data_inizio"])) ?? NULL;
		if(empty($data_inizio))
			$data_inizio = NULL;
		$data_fine = $db->realEscapeString(trim(@$_POST["data_fine"])) ?? NULL;
		if(empty($data_fine))
			$data_fine = NULL;

		if(!empty($titolo) && (!empty($tariffa) || $tariffa === "0"))
		{
			if($db->runQuery("INSERT INTO visite VALUES (NULL, '$titolo', " . (is_null($descrizione) ? "NULL" : "'$descrizione'") . ", $tariffa, " . (is_null($data_inizio) ? "NULL" : "'$data_inizio'") . ", " . (is_null($data_fine) ? "NULL" : "'$data_fine'") . ")") === false)
				echo "Errore";

			$db->closeConnection();
		}
		else
			echo "Errore";
	}
?>
