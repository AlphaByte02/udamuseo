<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(isset($_SESSION["user"]) && unserialize($_SESSION["user"])->haPrivilegio("A"))
		$user = unserialize($_SESSION["user"]);
	else
		header("Refresh: 3; url= " . _ROOT_DIR_ . "/");
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Statistiche</title>

		<!-- Librerie Highcharts -->
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/data.js"></script>
		<script src="https://code.highcharts.com/modules/series-label.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>
		<!-- Librerie Effetti HighCharts -->
		<script src="https://www.highcharts.com/media/com_demo/js/highslide-full.min.js"></script>
		<script src="https://www.highcharts.com/media/com_demo/js/highslide.config.js" charset="utf-8"></script>
		<link rel="stylesheet" type="text/css" href="https://www.highcharts.com/media/com_demo/css/highslide.css" />

		<style media="screen">
			.datefilter {
				display: inline;
			}

			.datefilter {
				width: auto !important;
			}

			#nativeDatePicker,
			#fallbackDatePicker {
				width: auto !important;
				margin-right: .5rem !important;
				display: inline;
			}

			@media only screen and (max-width: 1280px) {
				.datefilter {
					display: block;
					margin: 10px auto;
				}
				.datefilter {
					width: 75% !important;
				}
			}
		</style>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="admin";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<?php
					if(!isset($user)){
						echo "<h2>DEVI AVERE EFFETTUATO IL LOGIN ED ESSERE UN AMINISTRATORE PER ACCEDERE A QUESTA PAGINA!</h2></main>";
						include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
						die("</div></body></html>");
					}
				?>

				<h1>Statistiche Admin</h1>

				<div class="clearfix">
					<a class="btn btn-danger float-left" href="homeAdmin.php">Torna indietro</a>
				</div>
				<br/>
				<div class="clearfix mb-3">
					<div class="mx-auto" style="width: 84%">
						<div class="btn-group mb-3">
							<a href="statistiche.php?tab=t" role="button" class="btn <?php echo !isset($_GET["tab"]) || (isset($_GET["tab"]) && $_GET["tab"] == "t") ? "btn-primary" : "btn-outline-primary"?>">Totali</a>
							<a href="statistiche.php?tab=v" role="button" class="btn <?php echo isset($_GET["tab"]) && $_GET["tab"] == "v" ? "btn-primary" : "btn-outline-primary"?>">Per Esposizione</a>
						</div>
						<?php if(!isset($_GET["tab"]) || (isset($_GET["tab"]) && $_GET["tab"] == "t")): ?>
							<div class="dropdown mb-1">
								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
									<?php
										switch(@$_GET["filtro"])
										{
											case "se":
												echo "Solo Eventi";
												break;
											case "sv":
												echo "Solo Visite";
												break;
											default:
												echo "Eventi e Visite";
												break;
										}
									?>
								</button>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="statistiche.php?filtro=t<?php echo isset($_GET["dif"], $_GET["dff"]) ? "&dif=$_GET[dif]&dff=$_GET[dff]" : ""?>">Eventi e Visite</a>
									<a class="dropdown-item" href="statistiche.php?filtro=se<?php echo isset($_GET["dif"], $_GET["dff"]) ? "&dif=$_GET[dif]&dff=$_GET[dff]" : ""?>">Solo Eventi</a>
									<a class="dropdown-item" href="statistiche.php?filtro=sv<?php echo isset($_GET["dif"], $_GET["dff"]) ? "&dif=$_GET[dif]&dff=$_GET[dff]" : ""?>">Solo Visite</a>
								</div>
							</div>
							<form method="GET" class="form container" autocomplete="off">
								<input type="hidden" name="filtro" value="<?php echo isset($_GET["filtro"]) ? $_GET["filtro"] : "t" ?>">
								<p class="datefilter">Filtra gli elementi che iniziano tra</p>
								<input type="date" class="form-control datefilter" name="dif" value="<?php echo isset($_GET["dif"]) ? $_GET["dif"] : "" ?>" required/>
								<p class="datefilter">e il </p>
								<input type="date" class="form-control datefilter" name="dff" value="<?php echo isset($_GET["dff"]) ? $_GET["dff"] : "" ?>" required/>
								<button type="submit" class="btn btn-primary m-2">Cerca</button>
								<button type="reset" class="btn btn-primary m-2" onclick="location.href = location.pathname">Reset</button>
							</form>
						<?php elseif(isset($_GET["tab"]) && $_GET["tab"] == "v"): ?>
							<br/>
							<div id="nativeDatePicker">
								<label for="inpDate">Periodo:</label>
								<input type="month" id="inpDate" name="inpDate" class="form-control datefilter">
							</div>
							<div id="fallbackDatePicker">
								<label for="fallbackDateMonth">Month:</label>
								<select id="fallbackDateMonth" name="fallbackDateMonth" class="custom-select datefilter">
									<option selected value="-1">-Seleziona un Mese-</option>
									<option value="01">Gennaio</option>
									<option value="02">Febbraio</option>
									<option value="03">Marzo</option>
									<option value="04">Aprile</option>
									<option value="05">Maggio</option>
									<option value="06">Giugno</option>
									<option value="07">Luglio</option>
									<option value="08">Agosto</option>
									<option value="09">Settembre</option>
									<option value="10">Ottobre</option>
									<option value="11">Novembre</option>
									<option value="12">Dicembre</option>
			 					</select>
								<span>
									<label for="fallbackDateYear">Year:</label>
									<select id="fallbackDateYear" name="fallbackDateYear" class="custom-select datefilter">
									</select>
								</span>
							</div>
							<select disabled id="selectEsp" class="custom-select datefilter">
								<option selected>Nessuna Esposizione</option>
							</select>
						<?php endif; ?>
					</div>
				</div>
				<h5 id="nBigliettiEsposizione"></h5>
				<div id="containerChart" class="my-0 mx-auto" style="width: 65vw; height: 50vh"></div>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
					<?php if(!isset($_GET["tab"]) || (isset($_GET["tab"]) && $_GET["tab"] == "t")): ?>
						$.ajax({
							type: "POST",
							url: "ajax/r_statisticheTotali.php",
							data: {
								filtro: <?php echo isset($_GET["filtro"]) ? "'$_GET[filtro]'" : "'t'" ?>
								<?php
									if(isset($_GET["dif"], $_GET["dff"]) && !empty($_GET["dif"]) && !empty($_GET["dff"]))
									{
										echo ",dif: '$_GET[dif]', dff: '$_GET[dff]'";
									}
								?>
							},
							dataType: "script",
							cache: false,
							success: function(r, ts){
								if(ts === "success")
								{
									//console.log(r);
									console.log("Script loaded")
									if(r === "//error")
										$("#containerChart").html("<h3>Nessun Dato da Visualizzare</h3>")
								}
								else
									alert("La richiesta non è andata a buon fine, riprovare")
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						})
					<?php else: ?>
						$("#fallbackDatePicker").hide()
						var isFallBack = false

						var test = document.createElement("input");
						test.type = "month";
						if(test.type !== "month")
						{
							isFallBack = true

							$("#fallbackDatePicker").show()
							$("#nativeDatePicker").hide();

							var year = (new Date()).getFullYear();

							let start = -5
							for(let i = start; i <= (start * 3 * -1); i++)
							{
								var option = document.createElement("option")
								option.textContent = year - i
								if(i == 0)
									option.setAttribute("selected", "")
								$("#fallbackDateYear").append(option)
							}
						}

						$("#inpDate,#fallbackDateMonth,#fallbackDateYear").change(function(){
							var date = $(this).val()

							if(isFallBack)
							{
								if($("#fallbackDateMonth").val() == -1)
								{
									$("#selectEsp").prop("disabled", true)
									$("#selectEsp").html("<option selected>Nessuna Esposizione</option>")

									return
								}

								date = $("#fallbackDateYear").val() + "-" + $("#fallbackDateMonth").val()
							}

							if(date === "")
							{
								$("#selectEsp").prop("disabled", true)
								$("#selectEsp").html("<option selected>Nessuna Esposizione</option>")

								$("#containerChart").css("visibility", "hidden")
								$("#nBigliettiEsposizione").hide()

								return
							}

							$.ajax({
								type: "POST",
								url: "ajax/r_statisticheEsposizioni.php",
								data: { date: date },
								dataType: "html",
								cache: false,
								success: function(r, ts){
									if(ts === "success" && r !== "error" && r !== "norecord")
									{
										$("#selectEsp").prop("disabled", false)
										$("#selectEsp").html(r)
									}
									else if(ts === "success")
									{
										$("#selectEsp").prop("disabled", true)
										$("#selectEsp").html("<option selected>Nessuna Esposizione</option>")

										$("#containerChart").css("visibility", "hidden")
										$("#nBigliettiEsposizione").hide()
									}
								},
								error: function() {
									console.log("Errore")
									alert("La richiesta non è andata a buon fine, riprovare")
								}
							})
						})

						$("#selectEsp").change(function(){
							if($(this).val() != -1)
							{
								$("#containerChart").css("visibility", "visible")
								$.ajax({
									type: "POST",
									url: "ajax/r_statisticheEsposizioni.php",
									data: { idEsp: $(this).val() },
									dataType: "script",
									cache: false,
									success: function(r, ts){
										if(ts === "success")
										{
											//console.log(r);
											console.log("Script loaded")
											if(r === "//error")
											{
												$("#containerChart").html("<h3>Nessun Dato da Visualizzare</h3>");
												$("#nBigliettiEsposizione").hide()
											}
											else
											{
												$.ajax({
													type: "POST",
													url: "ajax/r_statisticheEsposizioniNBiglietti.php",
													data: { idEsp: $("#selectEsp").val() },
													dataType: "html",
													cache: false,
													success: function(r, ts){
														if(ts === "success")
														{
															if(r !== "error")
															{
																$("#nBigliettiEsposizione").show()
																$("#nBigliettiEsposizione").text("Il numero di biglietti venduti complessivamente per questa esposizione sono: " + r)
															}
															else
																$("#nBigliettiEsposizione").hide()
														}
														else
															alert("La richiesta non è andata a buon fine, riprovare")
													},
													error: function() {
														console.log("Errore")
														alert("La richiesta non è andata a buon fine, riprovare")
													}
												})
											}
										}
										else
											alert("La richiesta non è andata a buon fine, riprovare")
									},
									error: function() {
										console.log("Errore")
										alert("La richiesta non è andata a buon fine, riprovare")
									}
								})
							}
							else
							{
								$("#containerChart").css("visibility", "hidden")
								$("#nBigliettiEsposizione").hide()
							}
						})
					<?php endif; ?>
				})
			</script>
		</div>
	</body>
</html>
