<!--Modal Success-->
<div class="modal fade text-dark" id="modalSuccess" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Successo</h4>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="media p-3" data-toggle="modal" data-target="#modalAdd">
					<img src="<?php echo _ROOT_DIR_ . "/file/struct/immagini/success.png"; ?>" alt="success" class="mr-3 mt-3" style="width:60px;"/>
					<div class="media-body">
						<h4>Azione avvenuta con successo</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal Error-->
<div class="modal fade text-dark" id="modalError" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Errore</h4>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="media p-3" data-toggle="modal" data-target="#modalAdd">
					<img src="<?php echo _ROOT_DIR_ . "/file/struct/immagini/error.png"; ?>" alt="error" class="mr-3 mt-3" style="width:60px;"/>
					<div class="media-body">
						<h4>C'è stato un errore</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
