<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$email = trim($_POST["email_registrazione"]) ?? NULL;
		$password = trim($_POST["password_registrazione"]) ?? NULL;
		$conferma_password = trim($_POST["conferma_password_registrazione"]) ?? NULL;
		$cognome = trim($_POST["cognome_registrazione"]) ?? NULL;
		$nome = trim($_POST["nome_registrazione"]) ?? NULL;
		if(!empty($email) && !empty($password) && !empty($conferma_password) && !empty($cognome) && !empty($nome))
		{
			if($password === $conferma_password)
			{
				try
				{
					$user = User::registraUtente($email, $password, $cognome . " " . $nome);

					$_SESSION["user"] = serialize($user);
				}
				catch (\Exception $ex)
				{
					echo $ex->getMessage();
				}
			}
			else
				echo "Le due password devo coincidere!";
		}
		else
		{
			echo "Compila correttamente tutti i campi!";
		}
	}
?>
