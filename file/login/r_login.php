<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$email = trim(@$_POST["email_login"]) ?? NULL;
		$password = trim(@$_POST["password_login"]) ?? NULL;
		if(!empty($email) && !empty($password))
		{
			try
			{
				$user = new User($email, $password);

				//echo $user; // Solo per controllo

				$_SESSION["user"] = serialize($user);
			}
			catch (\Exception $ex)
			{
				echo $ex->getMessage();
			}
		}
		else
		{
			echo "Compita tutti i campi!";
		}
	}
?>
