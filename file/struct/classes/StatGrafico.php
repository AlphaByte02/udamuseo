<?php
	require_once __DIR__ . "/DB.php";

	abstract class StatGrafico
	{
		public static function ricavatoTotale($dif = "", $dff = "", $f = "t")
		{
			$db = new DB();

			$dif = $db->realEscapeString($dif);
			$dff = $db->realEscapeString($dff);

			$query = "SELECT data_emissione,SUM((tariffa * (100-c.sconto) / 100) + IFNULL((SELECT SUM(prezzo) FROM accessori JOIN servizi ON codice_servizio=codice WHERE accessori.id_biglietto=b.id_biglietto GROUP BY id_biglietto), 0)) AS 'costo' FROM biglietti AS b JOIN visite AS v ON codice_visita=v.codice JOIN categorie AS c ON codice_categoria=c.codice";
			switch ($f) {
				case "se":
					$query .= " WHERE v.data_inizio IS NOT NULL AND v.data_fine IS NOT NULL";
					if(!empty($dif) && !empty($dff))
						$query .= " AND data_emissione BETWEEN '$dif' AND '$dff'";
					break;
				case "sv":
					$query .= " WHERE v.data_inizio IS NULL AND v.data_fine IS NULL";
					if(!empty($dif) && !empty($dff))
						$query .= " AND data_emissione BETWEEN '$dif' AND '$dff'";
					break;
				default:
					if(!empty($dif) && !empty($dff))
						$query .= " WHERE data_emissione BETWEEN '$dif' AND '$dff'";
					break;
			}
			$query .= " GROUP BY b.data_emissione ORDER BY b.data_emissione ASC";

			$res = $db->runQuery($query);
			if($res !== false && $res->num_rows > 0)
			{
				$serie = "";
				while($el = $res->fetch_assoc())
				{
					if($serie != "")
						$serie .= ",";

					$data = new DateTime($el["data_emissione"]);
					$serie .= "[Date.UTC({$data->format("Y")}," . ($data->format("m")-1) . ",{$data->format("d")}), " . (sprintf("%04.2f", $el["costo"])) . "]";
				}

				$db->closeConnection();
				if(!empty($dif) && !empty($dff))
					return self::echoHighcharts("Ricavato Totale Con Filtro Data", "Ricavato Totale complessivo di esposizioni e visite ordinarie compresi gli accessori tra $dif e $dff", $serie);
				else
					return self::echoHighcharts("Ricavato Totale", "Ricavato Totale complessivo di esplosizioni e visite ordinarie compresi gli accessori", $serie);
			}
			else
				return false;

			$db->closeConnection();
		}

		public static function ricavatoEsposizione($idEsposizione)
		{
			$db = new DB();

			$idEsposizione = $db->realEscapeString($idEsposizione);

			$titoloEsp = ($db->runQuery("SELECT titolo FROM visite WHERE codice=$idEsposizione"))->fetch_assoc()["titolo"];

			$query = "SELECT data_emissione,SUM((tariffa * (100-c.sconto) / 100) + IFNULL((SELECT SUM(prezzo) FROM accessori JOIN servizi ON codice_servizio=codice WHERE accessori.id_biglietto=b.id_biglietto GROUP BY id_biglietto), 0)) AS 'costo' FROM biglietti AS b JOIN visite AS v ON codice_visita=v.codice JOIN categorie AS c ON codice_categoria=c.codice WHERE v.codice=$idEsposizione GROUP BY b.data_emissione ORDER BY b.data_emissione ASC";

			$res = $db->runQuery($query);
			if($res !== false && $res->num_rows > 0)
			{
				$serie = "";
				while($el = $res->fetch_assoc())
				{
					if($serie != "")
						$serie .= ",";

					$data = new DateTime($el["data_emissione"]);
					$serie .= "[Date.UTC({$data->format("Y")}," . ($data->format("m")-1) . ",{$data->format("d")}), " . (sprintf("%04.2f", $el["costo"])) . "]";
				}

				$db->closeConnection();

				return self::echoHighcharts("Ricavato Per $titoloEsp", "Ricavato compresi gli accessori ed aplicati gli sconti", $serie);
			}
			else
				return false;

			$db->closeConnection();
		}

		public static function echoHighcharts($titolo, $sottotitolo, $serie)
		{
			$titolo = str_replace("'", "\'", $titolo);
			$sottotitolo = str_replace("'", "\'", $sottotitolo);

			return "
			Highcharts.chart('containerChart', {
				chart: {
					type: 'spline',
					zoomType: 'x'
				},
				title: {
					text: '$titolo'
				},
				subtitle: {
					text: '$sottotitolo'
				},
				xAxis: {
					type: 'datetime',
					dateTimeLabelFormats: {
						month: '%e %b'
					},
					title: {
						text: 'Data'
					}
				},
				yAxis: {
					title: {
						text: 'Ricavato (€)'
					},
					min: 0
				},
				tooltip: {
					headerFormat: '<b>{series.name}</b><br>',
					pointFormat: '{point.x:%e %b}: {point.y:.2f} €'
				},
				plotOptions: {
					spline: {
						marker: {
							enabled: true
						},
						dataLabels: {
							enabled: true
						}
					}
				},
				colors: ['#6CF', '#39F', '#06C', '#036', '#000'],
				series: [
				{
					name: 'Eventi',
					data: [
						$serie
					]
				}]
			})";
		}
	}
?>
