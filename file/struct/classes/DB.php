<?php
	require_once __DIR__ . "/../Config.php";

	class DB extends Config
	{
		protected $conn;
		private $error;

		/** Costruttore, crea la connessione al database */
		public function __construct()
		{
			$this->conn = new mysqli($this->DB_HOST, $this->DB_USERNAME, $this->DB_PASSWD, $this->DB_NAME);
			if ($this->conn->connect_error)
				die("Connessione fallita: " . $this->conn->connect_error);
			$this->conn->set_charset('utf8mb4');
		}//__construct

		/**
		 * Esegue una query SQL
		 *
		 * @param string $sql query da eseguire
		 *
		 * @return bool|mysqli_result risultato della query in caso di successo altrimenti ritorna false
		 */
		public function runQuery($sql)
		{
			$result = $this->conn->query($sql);
			if ($result === false)
			{
				$this->error = $this->conn->error;
				return false;
			}
			else
				return $result;
		}//runQuery

		/**
		 * Ritorna l'ultimo errore dato
		 *
		 * @return bool ultimo errore dato
		 */
		public function getLastError()
		{
			if (!isset($this->error))
				return false;
			return $this->error;
		}//getLastError

		/**
		 * Esegue il real_escape_string su una stringa data
		 *
		 * @param string $string stringa su cui applicare la funzione
		 *
		 * @return string la stringa dopo aver applicato la funzione
		 */
		function realEscapeString($string)
		{
			return $this->conn->real_escape_string($string);
		}//realEscapeString

		/**
		 * Restituisce l'ultimo id inserito in una tabella
		 *
		 * @return int ultimo id inserito in una tabella
		 */
		function getInsertId()
		{
			return $this->conn->insert_id;
		}//getInsertId

		/**
		 * Inizia una transazione
		 *
		 * @return bool se è andata a buon fine
		 */
		function beginTransaction()
		{
			return $this->conn->begin_transaction();
		}//beginTransaction

		/**
		 * Chiude una transazione facendo un commit o un rollback
		 *
		 * @param bool $commit Se true esegue il commit altrimenti fa il rollback
		 *
		 * @return bool Il risultato dell'operazione eseguita
		 */
		function endTransaction($commit)
		{
			if ($commit)
				return $this->conn->commit();
			else
				return $this->conn->rollback();
		}//beginTransaction

		/** Chiude la connessione */
		public function closeConnection()
		{
			$this->conn->close();
		}//closeConnection
	}//Db

?>
