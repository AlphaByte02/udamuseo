<?php
	require_once __DIR__ . "/DB.php";

	class User
	{
		private $iduser;
		private $email;
		/*
			$email = array("id" , "email")
		*/
		private $cognomeNome;
		private $privilegio;
		/*
			A - Admin
			U - User
		*/

		/**
		 * Costruttore di User, eseguendo il login
		 *
		 * @param string $email    email dell'utente di qui fare il login
		 * @param string $password password dell'utente di qui fare il login
		 *
		 * @throws Exception
		 */
		public function __construct($email, $password)
		{
			$db = new DB();
			$email = trim($db->realEscapeString($email));

			$result = $db->runQuery("SELECT id,password,cod_email,cognome_nome,privilegi FROM utenti NATURAL JOIN email WHERE email='$email'");
			if ($result !== false && $result->num_rows == 1)
			{
				$user = $result->fetch_assoc();
				if (hash("sha256", (explode("@", $email)[0] . $password . explode("@", $email)[1])) == strtolower($user["password"]))
				{
					$this->iduser = $user["id"];
					$this->email = array("id" => $user["cod_email"], "email" => $email);
					$this->cognomeNome = $user["cognome_nome"];
					$this->privilegio = strtoupper($user["privilegi"]);
				}
				else
				{
					$db->closeConnection();
					throw new \Exception("Username o Password non valida!", 1);
				}
			}
			else if ($result !== false && $result->num_rows != 1)
			{
				$db->closeConnection();
				throw new \Exception("Username o Password non valida!", 1);
			}
			else
			{
				$db->closeConnection();
				throw new \Exception($db->getLastError(), 1);//TODO: Da togliere nella versione finale
			}

			$db->closeConnection();
		}

		/**
		 * Ritorna l'id dell'utente loggato
		 *
		 * @return int id dell'utente loggato
		 */
		public function getId()
		{
			return $this->iduser;
		}

		/**
		 * Ritorna la email dell'utente loggato
		 *
		 * @return string email dell'utente loggato
		 */
		public function getEmail()
		{
			return $this->email["email"];
		}

		/**
		 * Ritorna l'id della email dell'utente loggato
		 *
		 * @return int id della email dell'utente loggato
		 */
		public function getIdEmail()
		{
			return $this->email["id"];
		}

		/**
		 * Controlla se la password data corrisponde a quella messa nel database
		 *
		 * @param string $password la password da controllare
		 *
		 * @return bool se la password data corrisponde a quella messa nel database
		 */
		public function checkPassword($password)
		{
			$uguale = false;

			if(!empty($password))
			{
				$db = new DB();

				$password = $db->realEscapeString($password);

				$result = $db->runQuery("SELECT password FROM utenti WHERE id={$this->iduser}");
				if ($result !== false && $result->num_rows == 1)
				{
					$user = $result->fetch_assoc();
					if(hash("sha256", (explode("@", $this->email["email"])[0] . $password . explode("@", $this->email["email"])[1])) == strtolower($user["password"]))
						$uguale = true;
				}

				$db->closeConnection();
			}

			return $uguale;
		}

		/**
		 * Imposta una nuova email all'utente loggato
		 *
		 * @param string $email nuova email
		 * @param string $password password dell'utente loggato
		 *
		 * @throws Exception
		 */
		public function setEmail($email, $password)
		{
			$db = new DB();

			$email = trim($db->realEscapeString($email));
			$password = trim($db->realEscapeString($password));

			$res = $db->runQuery("SELECT COUNT(*) AS 'nEmail' FROM email WHERE email='$email'");
			if($res !== false)
			{
				if($res->num_rows > 0 && $res->fetch_assoc()["nEmail"] == 0)
				{
					if(!$this->checkPassword($password))
						throw new \Exception("La vecchia password è errata, riprovare", 1);

					$saltpassword = explode("@", $email)[0] . $password . explode("@", $email)[1];
					$newpassword = hash("sha256", $saltpassword);
					if($db->runQuery("UPDATE utenti SET password='$newpassword' WHERE id={$this->iduser}") !== false)
					{
						if($db->runQuery("UPDATE email SET email='$email' WHERE cod_email={$this->getIdEmail()}") !== false)
						{
							$this->email["email"] = $email;

							$db->closeConnection();
							return;
						}
						else
						{
							$db->closeConnection();
							throw new \Exception("C'è stato un errore nell'esecuzione del comando", 1);
						}
					}
					else
					{
						$db->closeConnection();
						throw new \Exception("C'è stato un errore nell'esecuzione del comando", 1);
					}
				}
				else
				{
					$db->closeConnection();
					throw new \Exception("La nuova email data esiste già, impossibile aggiornare", 1);
				}
			}
			else
			{
				$db->closeConnection();
				throw new \Exception("C'è stato un errore nell'esecuzione del comando", 1);
			}
		}

		/**
		 * Ritorna il cognomeNome dell'utente loggato
		 *
		 * @return string cognomeNome dell'utente loggato
		 */
		public function getCognomeNome()
		{
			return $this->cognomeNome;
		}

		/**
		 * Imposta un nuovo cognomeNome all'utente loggato
		 *
		 * @param string $cognomeNome nuovo cognomeNome
		 *
		 * @throws Exception
		 */
		public function setCognomeNome($cognomeNome)
		{
			$db = new DB();

			$cognomeNome = trim($db->realEscapeString($cognomeNome));

			if ($db->runQuery("UPDATE utenti SET cognome_nome='$cognomeNome' WHERE id=" . $this->getId()) !== false)
				$this->cognomeNome = $cognomeNome;
			else
			{
				$db->closeConnection();
				throw new \Exception("C'è stato un errore nell'esecuzione del comando", 1);
			}
		}

		/**
		 * Imposta una nuova password all'utente loggato
		 *
		 * @param string $oldpassword vecchia password
		 * @param string $newpassword nuova password
		 *
		 * @throws Exception
		 */
		public function setNewPassword($oldpassword, $newpassword)
		{
			$db = new DB();

			$oldpassword = trim($db->realEscapeString($oldpassword));
			$newpassword = trim($db->realEscapeString($newpassword));

			$result = $db->runQuery("SELECT password FROM utenti WHERE id={$this->iduser}");
			if ($result !== false && $result->num_rows == 1)
			{
				$user = $result->fetch_assoc();
				if($this->checkPassword($oldpassword))
				{
					if(self::checkCaratteriConsentiti($newpassword))
					{
						$saltpassword = explode("@", $this->email["email"])[0] . $newpassword . explode("@", $this->email["email"])[1];
						$newpassword = hash("sha256", $saltpassword);
						if($db->runQuery("UPDATE utenti SET password='$newpassword' WHERE id={$this->iduser}") !== false)
						{
							$db->closeConnection();
							return;
						}
						else
						{
							$db->closeConnection();
							throw new \Exception("C'è stato un errore nell'esecuzione del comando", 1);
						}
					}
					else
					{
						$db->closeConnection();
						throw new \Exception("Caratteri non validi all'interno della password!", 1);
					}
				}
				else
				{
					$db->closeConnection();
					throw new \Exception("La vecchia password è errata, riprovare", 1);
				}
			}
			else
			{
				$db->closeConnection();
				throw new \Exception("C'è stato un errore nell'esecuzione del comando", 1);
			}
		}

		/**
		 * Ritorna se l'utente loggato ha un determinato privilegio
		 *
		 * @param string $privilegio privilegio da controllare
		 *
		 * @return bool se l'utente loggato ha un determinato privilegio
		 */
		public function haPrivilegio($privilegio)
		{
			return $this->privilegio == strtoupper($privilegio);
		}

		/**
		 * Elimina l'utente dal database
		 *
		 * @return bool se l'azione è andata a buon fine
		 */
		public function eliminaProfilo()
		{
			$db = new DB();

			$res = $db->runQuery("DELETE FROM utenti WHERE id={$this->iduser}");

			if($res !== false)
			{
				$resRichieste = $db->runQuery("SELECT COUNT(*) AS 'nRichieste' FROM richieste WHERE cod_email={$this->email["id"]}");
				if($resRichieste !== false && $resRichieste->num_rows > 0)
					if($resRichieste->fetch_assoc()["nRichieste"] == 0)
						$res = $db->runQuery("DELETE FROM email WHERE cod_email={$this->email["id"]}");
			}

			$db->closeConnection();
			return $res;
		}

		/**
		 * toString della classe User
		 *
		 * @return string toString
		 */
		public function __toString()
		{
			return "ID: {$this->iduser}<br/>Email: {$this->email["email"]}<br/>Cognome-Nome: {$this->cognomeNome}<br/>Privilegio: {$this->privilegio}<br/>";
		}

		/**
		 * Registra un utente all'interno del database e gli fa il login in automatico
		 *
		 * @param string $email        email dell'utente che si vuole registrare
		 * @param string $password     password dell'utente che si vuole registrare
		 * @param string $cognome_nome cognome_nome dell'utente che si vuole registrare
		 *
		 * @return User nuovo utente appena creato
		 *
		 * @throws Exception
		 */
		public static function registraUtente($email, $password, $cognome_nome)
		{
			if (empty($email) || empty($password) || empty($cognome_nome))
				throw new \Exception("I campi non possono essere vuoti o nulli", 1);

			$db = new DB();

			$email = trim($db->realEscapeString($email));
			$password = $db->realEscapeString($password);
			$cognome_nome = trim($db->realEscapeString($cognome_nome));

			if (strlen($password) > 0)
			{
				if(!self::checkCaratteriConsentiti($password))
				{
					$db->closeConnection();
					throw new \Exception("Caratteri non validi all'interno della password!", 1);
				}
			}
			else
			{
				$db->closeConnection();
				throw new \Exception("La password non può essere vuota!", 1);
			}

			if ($db->runQuery("INSERT INTO email VALUES (NULL, '$email')") !== false)
			{
				$idEmail = $db->getInsertId();
				$saltpassword = explode("@", $email)[0] . $password . explode("@", $email)[1];
				$hashpassword = hash("sha256", $saltpassword);
				if ($db->runQuery("INSERT INTO utenti VALUES (NULL, $idEmail, '$hashpassword', '$cognome_nome', DEFAULT)") !== false)
				{
					$db->closeConnection();
					return new User($email, $password);
				}
				else
				{
					$db->closeConnection();
					throw new \Exception("Errore", 1);
				}
			}
			else
			{
				$db->closeConnection();
				throw new \Exception("Esiste già questa email", 1);
			}
		}

		/**
		 * Ritorna la stringa passata contiene solo caratteri consentiti
		 *
		 * @param string $string        stringa da analizzare
		 * @param string $chrConsentiti caratteri da consentire
		 *
		 * @return bool se la stringa passata contiene solo caratteri consentiti
		 */
		private static function checkCaratteriConsentiti($string, $chrConsentiti = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.#_")
		{
			return (count(array_diff(str_split(strtoupper($string)), str_split($chrConsentiti))) == 0);

			//Regex con problemi
			/*if(preg_match('#^\S[\w\-\.]+\S$#i', $password, $m))
			{
				return count($m) == 1;
			}
			else
				return false;*/
		}
	}

?>
