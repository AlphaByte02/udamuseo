<?php

	class Biglietto
	{
		private $prezzoBase;
		private $prezzoTotale;

		/*
			array(
				[idServizio] =>
					[prezzo]
					[descrizione]
				.
				.
				.
			)
		*/
		private $servizi;

		/**
		 * Biglietto constructor.
		 *
		 * @param float $prezzoBase prezzo base del biglietto
		 */
		function __construct($prezzoBase = 0.0)
		{

			$this->prezzoBase = $prezzoBase;
			$this->prezzoTotale = $prezzoBase;

			$this->servizi = array();
		}

		/**
		 * Aggiunge un servizio al bigletto
		 *
		 * @param int $idServizio     id del servizio da aggiungere
		 * @param int $prezzo         prezzo del servizio da aggiungere
		 * @param string $descrizione descrizione del servizio da aggiungere
		 */
		public function addServizio($idServizio, $prezzo, $descrizione = "")
		{
			if (!$this->haServizio($idServizio))
			{
				$this->servizi[$idServizio] = array("prezzo" => $prezzo, "descrizione" => $descrizione);

				$this->prezzoTotale += $prezzo;
			}
		}

		/**
		 * Rimuove un servizio dato il suo id
		 *
		 * @param int $idServizio id del servizio da rimuovere
		 *
		 * @throws Exception
		 */
		public function removeServizio($idServizio)
		{
			if ($this->haServizio($idServizio))
			{
				$this->prezzoTotale -= $this->servizi[$idServizio]["prezzo"];
				unset($this->servizi[$idServizio]);
			}
			else
				throw new \Exception("Non Esiste Questo Servizio in Questo Biglietto", 1);
		}

		/**
		 * Rimuove tutti i servizi nel biglietto
		 */
		public function removeAllServizi()
		{
			$this->prezzoTotale = $this->prezzoBase;
			unset($this->servizi);
			$this->servizi = array();
		}

		/**
		 * Restituisce un servizio dato il suo id
		 *
		 * @param int $idServizio id del servizio da ritornare
		 *
		 * @return array array del servizio
		 * @throws Exception
		 */
		public function getServizio($idServizio)
		{
			if ($this->haServizio($idServizio))
				return $this->servizi[$idServizio];
			else
				throw new \Exception("Non Esiste Questo Servizio in Questo Biglietto", 1);
		}

		/**
		 * Restituisce il numero di servizi nel biglietto
		 *
		 * @return int numero di servizi nel biglietto
		 */
		public function getNServizi()
		{
			if (is_null($this->getServizi()))
				return 0;

			return count($this->getServizi());
		}

		/**
		 * Restituisce se il biglietto ha un determinato servizio
		 *
		 * @param int $idServizio id del servizio da cercare
		 *
		 * @return bool se il biglietto ha un determinato servizio
		 */
		public function haServizio($idServizio)
		{
			return isset($this->servizi[$idServizio]) || key_exists($idServizio, $this->servizi);
		}

		/**
		 * Restituisce tutti i servizi
		 *
		 * @return array|null
		 */
		public function getServizi()
		{
			if (!empty($this->servizi))
				return $this->servizi;
			else
				return NULL;
		}

		/**
		 * Restituisce il prezzo base del biglietto
		 *
		 * @return int prezzo base del biglietto
		 */
		public function getPrezzoBase()
		{
			return $this->prezzoBase;
		}

		/**
		 * Restituisce il prezzo base del biglietto più tutti i suoi servizi
		 *
		 * @return int prezzo base del biglietto più tutti i suoi servizi
		 */
		public function getPrezzoTotale()
		{
			return $this->prezzoTotale;
		}

		/**
		 * toString della classe Biglietto
		 *
		 * @return string toString
		 */
		public function __toString()
		{
			$st = "Prezzo Base: {$this->prezzoBase}<br/>Prezzo Finale: {$this->prezzoFinale}\n";
			return $st . print_r($this->servizi, true);
		}
	}

?>
