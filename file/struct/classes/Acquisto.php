<?php
	require_once __DIR__ . "/Biglietto.php";

	class Acquisto
	{
		private $idEvento;
		private $titoloEvento;

		private $prezzoEvento;

		/* TODO : Creare un indice che contiene tutti i biglietti di una certa riduzione
			array(
				[idRiduzione] =>
					[descrizione]
					[OBJ::Biglietto]
				.
				.
				.
			)
		*/
		private $biglietti;

		/**
		 * Acquisto constructor.
		 *
		 * @param int $idEvento        id dell'evento/visita
		 * @param float $prezzoEvento  prezzo dell'evento/visita
		 * @param string $titoloEvento titolo dell'evento/visita
		 */
		function __construct($idEvento, $prezzoEvento, $titoloEvento = "")
		{
			$this->idEvento = $idEvento;
			$this->titoloEvento = $titoloEvento;
			$this->prezzoEvento = $prezzoEvento;

			$this->biglietti = array();
		}

		/**
		 * Aggiunge un bigletto all'acquisto
		 *
		 * @param int $idRiduzione       id della riduzione da applicare
		 * @param float $scontoRiduzione sconto della riduzione da applicare
		 * @param string $descrizione    descrizione della riduzione
		 * @param int $quantita          quantita di biglietti da aggiungere con le stesse caratteristiche
		 */
		public function addBiglietto($idRiduzione, $scontoRiduzione, $descrizione = "", $quantita = 1)
		{
			if (!$this->haRiduzione($idRiduzione))
				$this->biglietti[$idRiduzione]["descrizione"] = $descrizione;

			for ($i = 0; $i < $quantita; $i++)
			{
				$this->biglietti[$idRiduzione][] = new Biglietto($this->prezzoEvento * (100 - $scontoRiduzione) / 100);
			}
		}

		/**
		 * Restituisce i bigletti acquistati
		 *
		 * @return array bigletti acquistati
		 */
		public function getBiglietti()
		{
			return $this->biglietti;
		}

		/**
		 * Restituisce tutti i biglietti con una certa riduzione applicata
		 *
		 * @param int $idRiduzione id della riduzione da cercare
		 *
		 * @return array array contenente i Biglietti con una certa riduzione applicata
		 * @throws Exception
		 */
		public function getBigliettiRiduzione($idRiduzione)
		{
			if ($this->haRiduzione($idRiduzione))
				return array_filter($this->biglietti[$idRiduzione], "is_int", ARRAY_FILTER_USE_KEY);
			else
				throw new \Exception("Non Esiste Questa Riduzione in Questo Acquisto", 1);
		}

		/**
		 * Restituisce la descrizione di una certa riduzione
		 *
		 * @param int $idRiduzione id della riduzione da cercare
		 *
		 * @return string descrizione di una certa riduzione
		 * @throws Exception
		 */
		public function getDescrizioneRiduzione($idRiduzione)
		{
			if ($this->haRiduzione($idRiduzione))
				return $this->biglietti[$idRiduzione]["descrizione"];
			else
				throw new \Exception("Non Esiste Questa Riduzione in Questo Acquisto", 1);
		}

		/**
		 * Aggiunge un servizio ad un determinato biglietto, se non è specificato lo aggiunge al primo idoneo
		 *
		 * @param int $idRiduzione            id della riduzione del biglietto su cui aggiungere il servizio
		 * @param int $idServizio             id del servizio da aggiungere
		 * @param float $prezzoServizio       prezzo del servizio da aggiungere
		 * @param string $descrizioneServizio descrizione del servizio da aggiungere
		 * @param int $idBiglietto            id del biglietto su cui aggiungere il servizio, se l'id è minore di 0 lo aggiunge al primo idoneo
		 *
		 * @throws Exception
		 */
		public function addServizio($idRiduzione, $idServizio, $prezzoServizio, $descrizioneServizio = "", $idBiglietto = -1)
		{
			try
			{
				if ($idBiglietto < 0)
				{
					foreach ($this->getBigliettiRiduzione($idRiduzione) as $b)
					{
						if (!$b->haServizio($idServizio))
						{
							$b->addServizio($idServizio, $prezzoServizio, $descrizioneServizio);
							break;
						}
					}
				}
				else
				{
					if ($this->haRiduzione($idRiduzione) && (isset($this->biglietti[$idRiduzione][$idBiglietto]) || key_exists($idBiglietto, $this->biglietti[$idRiduzione])))
					{
						$b = $this->getBigliettiRiduzione($idRiduzione)[$idBiglietto];
						if (!$b->haServizio($idServizio))
							$b->addServizio($idServizio, $prezzoServizio, $descrizioneServizio);
						else
							throw new \Exception("Questo servizio è già stato aggiunto a questo biglietto", 1);
					}
					else
						throw new \Exception("Questo biglietto non esiste", 1);
				}

			} catch (\Exception $ex)
			{
				throw new \Exception($ex->getMessage(), 1);
			}
		}

		/**
		 * Restituisce tutti i servizi di tutti i biglietti di una certa riduzione
		 *
		 * @param int $idRiduzione id della riduzione
		 *
		 * @return array array di tutti i servizi di tutti i biglietti di una certa riduzione
		 * @throws Exception
		 */
		public function getAllServiziRiduzione($idRiduzione)
		{
			$a = array();
			foreach ($this->getBigliettiRiduzione($idRiduzione) as $b)
			{
				if (!empty($b->getServizi()))
					$a[] = $b->getServizi();
			}

			return $a;
		}

		/**
		 * Restituisce tutti i servizi di tutti i biglietti di una certa riduzione restituendone solo un elemento per tipo di servizio
		 *
		 * @param int $idRiduzione id della riduzione
		 *
		 * @return array array di tutti i servizi di tutti i biglietti di una certa riduzione restituendone solo un elemento per tipo di servizio
		 * @throws Exception
		 */
		public function getAllServiziRiduzioneMerge($idRiduzione)
		{
			$a = array();
			foreach ($this->getBigliettiRiduzione($idRiduzione) as $b)
			{
				if (!empty($b->getServizi()))
					foreach ($b->getServizi() as $idServ => $serv)
					{
						$a[$idServ] = $serv;
					}
			}

			return $a;
		}

		/**
		 * Ritorna il numero di volte che è presente un servizio di una certa riduzione
		 *
		 * @param int $idRiduzione id della riduzione su cui cercare
		 * @param int $idServizio  id del servizio di cui cercare
		 *
		 * @return int numero di volte che è presente un servizio di una certa riduzione
		 * @throws Exception
		 */
		public function getNServizioRiduzione($idRiduzione, $idServizio)
		{
			$s = 0;

			foreach ($this->getAllServiziRiduzione($idRiduzione) as $serv)
			{
				if (in_array($idServizio, array_keys($serv)))
					$s++;
			}

			return $s;
		}

		/**
		 * Ritorna il numero di servizi in una determinata riduzione
		 *
		 * @param int $idRiduzione id della riduzione su cui cercare
		 *
		 * @return int numero di servizi in una determinata riduzione
		 * @throws Exception
		 */
		public function getNServiziRiduzione($idRiduzione)
		{
			return count($this->getAllServiziRiduzione($idRiduzione));

			/*$s = 0;

			foreach($this->getBigliettiRiduzione($idRiduzione) as $b)
				$s += $b->getNServizi();

			return $s;*/
		}

		/**
		 * Rimuove tutti i servizi di tutti i biglietti di una riduzione data
		 *
		 * @param int $idRiduzione id della riduzione da cui eliminare i servizi
		 *
		 * @throws Exception
		 */
		public function removeAllServiziRiduzione($idRiduzione)
		{
			foreach ($this->getBigliettiRiduzione($idRiduzione) as $b)
			{
				$b->removeAllServizi();
			}
		}

		/**
		 * Ritorna il numero di biglietti di una riduzione
		 *
		 * @param int $idRiduzione id della riduzione su cui cercare
		 *
		 * @return int numero di biglietti di una riduzione
		 * @throws Exception
		 */
		public function getNBigliettiRiduzione($idRiduzione)
		{
			return count($this->getBigliettiRiduzione($idRiduzione));
		}

		/**
		 * Ritorna il numero di riduzioni presenti nell'acquisto
		 *
		 * @return int numero di riduzioni presenti nell'acquisto
		 */
		public function getNRiduzioni()
		{
			return count($this->biglietti);
		}

		/**
		 * Ritorna il numero dei biglietti presenti in tutte le riduzioni
		 *
		 * @return int numero dei biglietti presenti in tutte le riduzioni
		 * @throws Exception
		 */
		public function getNAllBiglietti()
		{
			$s = 0;

			foreach ($this->getKeyRiduzioni() as $idRid)
			{
				$s += $this->getNBigliettiRiduzione($idRid);
			}

			return $s;
		}

		/**
		 * Ritorna se l'acquisto ha una determinata riduzione
		 *
		 * @param int $idRiduzione id della riduzione da cercare
		 *
		 * @return bool se l'acquisto ha una determinata riduzione
		 */
		public function haRiduzione($idRiduzione)
		{
			return isset($this->biglietti[$idRiduzione]) || key_exists($idRiduzione, $this->biglietti);
		}

		/**
		 * Ritorna l'id dell'evento
		 *
		 * @return int id dell'evento
		 */
		public function getIdEvento()
		{
			return $this->idEvento;
		}

		/**
		 * Ritorna il titolo dell'evento
		 *
		 * @return string titolo dell'evento
		 */
		public function getTitoloEvento()
		{
			return $this->titoloEvento;
		}

		/**
		 * Ritorna il prezzo dell'evento
		 *
		 * @return float prezzo dell'evento
		 */
		public function getPrezzoEvento()
		{
			return $this->prezzoEvento;
		}

		/**
		 * Ritorna gli id delle riduzioni presenti
		 *
		 * @return array array degli id delle riduzioni presenti
		 */
		public function getKeyRiduzioni()
		{
			return array_keys($this->biglietti);
		}

		/**
		 * Ritorna la somma del prezzo di tutti i biglietti senza contare i servizi
		 *
		 * @return int somma del prezzo di tutti i biglietti senza contare i servizi
		 * @throws Exception
		 */
		public function getPrezzoSoloBiglitti()
		{
			$s = 0;

			foreach ($this->getKeyRiduzioni() as $idRid)
			{
				foreach ($this->getBigliettiRiduzione($idRid) as $b)
				{
					$s += $b->getPrezzoBase();
				}
			}

			return $s;
		}

		/**
		 * Ritorna il prezzo totale di una singola riduzione
		 *
		 * @param int $idRiduzione id della riduzione
		 *
		 * @return int prezzo totale di una singola riduzione
		 * @throws Exception
		 */
		public function getPrezzoRiduzione($idRiduzione)
		{
			$s = 0;

			foreach ($this->getBigliettiRiduzione($idRiduzione) as $b)
			{
				$s += $b->getPrezzoTotale();
			}

			return $s;
		}

		/**
		 * Ritorna il prezzo totale dell'acquisto
		 *
		 * @return int prezzo totale dell'acquisto
		 * @throws Exception
		 */
		public function getPrezzoTotale()
		{
			$s = 0;

			foreach ($this->getKeyRiduzioni() as $idRid)
			{
				$s += $this->getPrezzoRiduzione($idRid);
			}

			return $s;
		}

		/**
		 * toString del la classe Acquisto
		 *
		 * @return string toString
		 * @throws Exception
		 */
		public function __toString()
		{
			$st = "IdEvento: {$this->idEvento}\n";
			$st .= "Prezzo Evento: {$this->prezzoEvento}, Prezzo Finale: {$this->getPrezzoTotale()}\n";
			return $st . print_r($this->biglietti, true);
		}
	}

?>
