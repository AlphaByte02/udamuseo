<?php
	if(!defined(@_ROOT_DIR_))
		require_once 'defineConstant.php';
?>

<meta charset="utf-8">
<meta name="theme-color" content="#222"/>
<link rel="icon" href="<?php echo _ROOT_DIR_ . "/file/struct/immagini/favicon.png" ?>" type="image/x-icon"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--link href="<?php echo _ROOT_DIR_ . "/bootstrap/css/bootstrap.min.css" ?>" rel="stylesheet"-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!--script type="text/javascript" src="<?php echo _ROOT_DIR_ . "/jquery/jquery-3.3.1.min.js" ?>"></script-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" type="text/javascript"></script>

<!--script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" type="text/javascript"></script>

<link href="<?php echo _ROOT_DIR_ . "/file/struct/style/mainStyle.css" ?>" rel="stylesheet">

<script src="<?php echo _ROOT_DIR_ . "/file/struct/js/functions.js" ?>" type="text/javascript"></script>

<!--link href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" rel="stylesheet"-->
