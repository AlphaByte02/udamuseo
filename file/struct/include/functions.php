<?php
	require_once "defineConstant.php";

	spl_autoload_register(function ($class)
	{
		$p = __DIR__ . "/../classes/$class.php";
		if(!file_exists($p))
		{
			$p = __DIR__ . "/../classes/$class/$class.php";
			if(!file_exists($p))
				throw new \Exception("Class Not Found", 1);
		}

		require_once $p;

		clearstatcache();
	});

	function encodeUrl($str)
	{
		$chr = array("#", "&", "?", " ");
		$rep = array("%23", "%26", "%3F", "%20");

		return str_replace($chr, $rep, $str);
	}

	function decodeUrl($str)
	{
		$chr = array("%23", "%26", "%3F", "%20");
		$rep = array("#", "&", "?", " ");

		return str_replace($chr, $rep, $str);
	}

	function contains($string, $comparator)
	{
		if(strpos($string, $comparator) === false && strpos($string, $comparator) !== 0)
			return false;
		else
			return true;
	}

	function endsWith($needle, $haystack)
	{
		$length = strlen($needle);
		return $length === 0 || (substr($haystack, -$length) === $needle);
	}

	function startsWith($needle, $haystack)
	{
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}

	function alert($msg)
	{
		echo "<script type='text/javascript'>alert(\"" . $msg . "\");</script>";
	}

	function getNameMonthIta($numMonth)
	{
		switch ($numMonth) {
			case 1:
				return "Gennaio";
				break;
			case 2:
				return "Febbraio";
				break;
			case 3:
				return "Marzo";
				break;
			case 4:
				return "Aprile";
				break;
			case 5:
				return "Maggio";
				break;
			case 6:
				return "Giugno";
				break;
			case 7:
				return "Luglio";
				break;
			case 8:
				return "Agosto";
				break;
			case 9:
				return "Settembre";
				break;
			case 10:
				return "Ottobre";
				break;
			case 11:
				return "Novembre";
				break;
			case 12:
				return "Dicembre";
				break;
			default:
				return "Gennaio";
				break;
		}
	}

	// check if a server is up by connecting to a port
	function chkServer($host, $port)
	{

		$hostip = @gethostbyname($host); // resloves IP from Hostname returns hostname on failure

		/*if ($hostip == $host) // if the IP is not resloved
		{
			return "Server is down or does not exist";
		}
		else
		{*/
			if (!$x = @fsockopen($hostip, $port, $errno, $errstr, 1)) // attempt to connect
			{
				return false;
			}
			else
			{
				if ($x)
				{
					@fclose($x); //close connection
				}
				return true;
			}
		//}
	}
?>
