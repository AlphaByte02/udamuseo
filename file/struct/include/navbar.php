<?php $pagina = $pagina ?? "" ?>
<header>
	<nav class="navbar navbar-expand-md bg-dark navbar-dark">
		<a class="navbar-brand" href="<?php echo $pagina == "index" ? "#" : _ROOT_DIR_ . "/index.php" ?>">Severi Museo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="<?php echo $pagina == "index" ? "#" : _ROOT_DIR_ . "/index.php" ?>" class="nav-link <?php echo $pagina == "index" ? "active" : "" ?>">Home</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo _ROOT_DIR_ . "/file/pagine/eventi.php" ?>" class="nav-link <?php echo $pagina == "eventi" ? "active" : "" ?>">Eventi</a>
				</li>
				<li class="nav-item">
					<a href="<?php echo $pagina == "about" ? "#" : _ROOT_DIR_ . "/file/pagine/about.php" ?>" class="nav-link <?php echo $pagina == "about" ? "active" : "" ?>">Chi siamo</a>
				</li>
				<?php if(isset($_SESSION["user"]))
					echo "<li class='nav-item'><a href=" . ($pagina == "utente" ? "#" : _ROOT_DIR_ . "/file/pagine/gestioneUtente.php") . " class='nav-link " . ($pagina == "utente" ? "active" : "") . "'>Utente</a></li>";
				?>
				<?php if(isset($_SESSION["user"]) && unserialize($_SESSION["user"])->haPrivilegio("A"))
					echo "<li class='nav-item'><a href=" . _ROOT_DIR_ . "/file/admin/homeAdmin.php" . " class='nav-link " . ($pagina == "admin" ? "active" : "") . "'>Admin</a></li>";
				?>
				<?php
					if(!isset($_SESSION["user"]))
						echo "<li class='nav-item'><a class='nav-link' href='#' data-toggle='modal' data-target='#modalLogin'>Login</a></li>";
					else
						echo "<li class='nav-item'><a href='#' id='logout' class='nav-link'>Logout</a></li>";
				?>
				<script type="text/javascript">
					$("#logout").click(function() {
						$.ajax({
							type: 'POST',
							url: <?php echo "'"._ROOT_DIR_."/file/login/r_logout.php'"?>,
							dataType: "html",
							cache: false,
							complete: function(r, ts){
								if(r.responseText == "" && ts === "success")
									location.href = <?php echo "'" . _ROOT_DIR_ . "/'"; ?>
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						});
					})
				</script>
			</ul>
		</div>
	</nav>
</header>

<!-- Modal Login-->
<div class="modal fade" id="modalLogin">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h3 class="modal-title">Login</h3>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form id="form_login" class="form container-fluid" method="POST" autocomplete="off">
					<div class="form-group">
						<h6>E-mail:</h6>
						<input type="email" class="form-control" name="email_login" id="email_login" required>
					</div>
					<div class="form-group">
						<h6>Password:</h6>
						<input type="password" class="form-control" name="password_login" id="password_login" required>
					</div>
					<p class="text-danger" id="login_error"></p>
					<button type="submit" id="login_button" class="btn btn-secondary mybtn-secondary btn-block">Login</button>
				</form>
				<br>
				<div class="container-fluid text-center">
					<!--a class="small" href="#" style="color: gray;">Password dimenticata?</a-->
					<p>Non sei ancora registrato?</p>
					<button type="button" id="modalButtonRegistrazione" class="btn btn-secondary mybtn-secondary btn-block" data-toggle="modal" data-target="#modalRegistrazione">Registrati Ora!</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Ajax Login-->
<script type="text/javascript">
	$('#modalButtonRegistrazione').click(function() {
		$('#modalLogin').modal('hide');
	});

	$("#form_login").submit(function(e){
		e.preventDefault()
		let serializeData = $(this).serialize()

		$.ajax({
			type: 'POST',
			url: <?php echo "'"._ROOT_DIR_."/file/login/r_login.php'"?>,
			data: serializeData,
			dataType: "html",
			cache: false,
			complete: function(r, ts){
				if(r.responseText == "" && ts === "success")
					location.reload()
				else
					$('#login_error').text(r.responseText);
			},
			error: function() {
				console.log("Errore")
				alert("La richiesta non è andata a buon fine, riprovare")
			}
		});
	})
</script>

<!-- Modal Registrazione-->
<div class="modal fade" id="modalRegistrazione">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h3 class="modal-title">Registrazione</h3>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<form id="form_registra" class="form container-fluid" method="POST">
					<div class="form-group">
						<h6>Cognome<span class="obbligatorio">*</span>:</h6>
						<input class="form-control" type="text" name="cognome_registrazione" id="cognome_registrazione" placeholder="Rossi" required>
						<h6>Nome<span class="obbligatorio">*</span>:</h6>
						<input class="form-control" type="text" name="nome_registrazione" id="nome_registrazione" placeholder="Mario" required>
					</div>
					<div class="form-group">
						<h6>Email<span class="obbligatorio">*</span>: </h6>
						<input class="form-control" type="email" name="email_registrazione" id="email_registrazione" placeholder="example@example.it" required>
					</div>
					<div class="form-group">
						<h6 for="password_registrazione">Password<span class="obbligatorio">*</span>:</h6>
						<input class="form-control" type="password" name="password_registrazione" id="password_registrazione" placeholder="Password..." required>
					</div>
					<div class="form-group">
						<h6>Conferma Password<span class="obbligatorio">*</span>:</h6>
						<input class="form-control" type="password" name="conferma_password_registrazione" id="conferma_password_registrazione" placeholder="Conferma Password..." required>
					</div>
					<p class="text-danger" id="registra_error"></p>
					<a class="small" href="<?php echo _ROOT_DIR_ . "/file/pagine/policy.php"; ?>" style="color: gray;">Registrandoti accetti le nostre policy</a>
					<button type="submit" id="registra_button" class="btn btn-secondary mybtn-secondary btn-block">Registrati</button>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Ajax registrazione-->
<script type="text/javascript">
	$("#form_registra").submit(function(e){
		e.preventDefault()
		let serializeData = $(this).serialize()

		$.ajax({
			type: 'POST',
			url: <?php echo "'"._ROOT_DIR_."/file/login/r_registrazione.php'"?>,
			data: serializeData,
			dataType: "html",
			cache: false,
			complete: function(r, ts){
				if(r.responseText == "" && ts === "success")
					location.reload();
				else
					$('#registra_error').text(r.responseText);
			},
			error: function() {
				console.log("Errore")
				alert("La richiesta non è andata a buon fine, riprovare")
			}
		});
	})
</script>
