<footer class="mt-auto /*cyan*/">
	<div class="my-2">
		<h6 class="mb-1">Severi Museo</h6>
		<a class="small" target="_blank" href="<?php echo _ROOT_DIR_ . "/file/pagine/policy.php"; ?>" style="color: gray;">Visualizza le nostre policy</a>
		-
		<a class="small" target="_blank" href="https://gitlab.com/AlphaByte02/udamuseo" style="color: gray;">Progetto su GitLab</a>
	</div>
</footer>
