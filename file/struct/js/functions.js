function zeroFill(number, width)
{
	width -= number.toString().length;
	if ( width > 0 )
	{
		return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
	}
	return number + ""; // always return a string
}

function roundDecimal(num, n)
{
	return parseFloat(Math.round(num * 10**n) / 10**n).toFixed(n)
}
