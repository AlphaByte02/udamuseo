<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(!isset($_SESSION["acquisto"]))
		header("Location: ". _ROOT_DIR_ . "/file/pagine/eventi.php");

	$acquisto = unserialize($_SESSION["acquisto"]);

	$db = new DB();
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Riepilogo Biglietteria</title>

		<style media="screen">
			tbody tr td {
				vertical-align: middle !important;
			}

			#div_result {
				display: none;
				max-width: 60%;
				margin: 20px auto;
			}

			@media only screen and (max-width: 767px) {
				#div_result {
					max-width: 100%;
				}
			}
		</style>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="eventi";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<h3 id="text_result"></h3>
				<div id="div_result" class="clearfix">
					<a class="btn btn-primary float-left" role="button" href="bigliettoPDF.php">Stampa PDF</a>
					<?php
						if(isset($_SESSION["user"]))
							echo "<a class='btn btn-primary float-right' href='gestioneUtente.php'>Vai alla Pagina di Gestione Utente</a>";
						else
							echo "<a class='btn btn-primary float-right' href='" . _ROOT_DIR_ . "/'>Vai alla HomePage</a>";
					?>
				</div>
				<div class="card">
					<div class="card-body">
						<h4 class="card-title" style='color:black;'>Riepilogo Acquisto</h4>
						<h5 class="card-title" style='color:black;'><strong><?php echo $acquisto->getTitoloEvento(); ?></strong></h5>
						<br/>
						<form id="form_riepilogo" method="POST" autocomplete="off">
							<div class="table-responsive">
								<table class="table table-dark table-bordered table-striped">
									<thead>
										<tr>
											<th>#</th>
											<th>Quantità</th>
											<th>Aggiunte</th>
											<th>Prezzo (&euro;)</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach ($acquisto->getKeyRiduzioni() as $idRid)
											{
												echo "<tr>";
													echo "<td>{$acquisto->getDescrizioneRiduzione($idRid)}</td>";
													echo "<td>{$acquisto->getNBigliettiRiduzione($idRid)}</td>";
													if($acquisto->getNServiziRiduzione($idRid))
													{
														echo "<td>";
															foreach ($acquisto->getAllServiziRiduzioneMerge($idRid) as $idServ => $serv)
															{
																echo "<p>$serv[descrizione] (x{$acquisto->getNServizioRiduzione($idRid, $idServ)})</p>";
															}
														echo "</td>";
													}
													else
														echo "<td>Nessuna Aggiunta</td>";
													echo "<td class='zf'>{$acquisto->getPrezzoRiduzione($idRid)}</td>";
												echo "</tr>";
											}

											echo "<tr><th>Riepilogo</th><th>{$acquisto->getNAllBiglietti()}</th><th>↑</th><th class='zf'>{$acquisto->getPrezzoTotale()}</th></th>";
										?>
									</tbody>
								</table>
							</div>
							<button type="button" id="button_annulla" class="btn btn-danger">Annulla Tutto</button>
							<button type="submit" id="button_acquista" class="btn btn-primary">Acquista</button>
						</form>
					</div>
				</div>
				<?php $db->closeConnection(); ?>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
					$(".zf").each(function(){
						$(this).text(zeroFill(roundDecimal(parseFloat($(this).text()), 2), 2))
					})

					$("#form_riepilogo").submit(function(e)
					{
						e.preventDefault()
						$.ajax({
							type: 'POST',
							url: "ajax/r_concludiAcquisto.php",
							dataType: "html",
							cache: false,
							complete: function(r, ts){
								if(r.responseText == "" && ts === "success")
								{
									$("div.card").hide()
									$("div#div_result").show()

									$("#text_result").text("Acquisto avvenuto con successo")
									$("#text_result").css("color", "green")

									/*setTimeout(function(){
										location.href = "<?php echo isset($_SESSION["user"]) ? "gestioneUtente.php" : _ROOT_DIR_ . "/"; ?>"
									}, 1500);*/
								}
								else
								{
									$("div.card").hide()
									$("#text_result").text(r.responseText)

									$("#text_result").css("color", "red")
								}
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						})
					})

					$("#button_annulla").click(function(){
						$.ajax({
							type: 'POST',
							url: "ajax/r_annullaAcquisto.php",
							dataType: "html",
							cache: false,
							complete: function()
							{
								location.href = "<?php echo _ROOT_DIR_ . "/"; ?>"
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						})
					})
				})
			</script>
		</div>
	</body>
</html>
