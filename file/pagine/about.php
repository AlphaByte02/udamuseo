<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
	
		<!-- SEO -->
		<title>Contatti e Recapiti | Museo Storico del Severi a Padova</title>
		<meta name="description" content="Contatti e Recapiti del Museo Storico del Severi a Padova">
		<meta property="og:title" content="Contatti e Recapiti | Museo Storico del Severi a Padova">
		<meta property="og:image" content="<?php echo _ROOT_DIR_ . '/file/struct/immagini/favicon.png';?>">
		<meta property="og:site_name" content="Severi Museo">
	
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="about";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<h1>Chi siamo?</h1>
				<p>Un gruppo di studenti della classe 5IA dell' istituto tecnico F.Severi di Padova impegnati in un progetto scolastico.</p>
				<br/>
				<div style="overflow:hidden;width: 100%;position: relative;">
					<iframe width="100%" height="300" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=it&amp;q=iti%20severi%20padova+(Severi)&amp;ie=UTF8&amp;t=&amp;z=11&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					<!-- Crediti ? -->
					<!--div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;">
						<small style="line-height: 1.8;font-size: 2px;background: #fff;">Powered by <a href="https://embedgooglemaps.com/fr/">https://embedgooglemaps.com/fr/</a> & <a href="https://newyorkhoponhopoffbus.nl">new york bus tours | hop on hop off bus</a></small>
					</div>
					<style>
						#gmap_canvas img{max-width:none!important;background:none!important}
					</style-->
				</div>
				<br/>
				<h1>Contattaci</h1>
				<form id="form_contatto" method="POST">
					<div class="form-group">
						<label for="email_contatto">Email<span class="obbligatorio">*</span>:</label>
						<input type="email" class="form-control" id="email_contatto" name="email_contatto" placeholder="name@example.it" value="<?php echo isset($_SESSION["user"]) ? (unserialize($_SESSION["user"]))->getEmail() : "" ?>" <?php echo isset($_SESSION["user"]) ? "readonly" : "" ?> required>
					</div>
					<div class="form-group">
						<label for="oggetto_contatto">Oggetto<span class="obbligatorio">*</span>:</label>
						<select class="form-control" id="oggetto_contatto" name="oggetto_contatto" required>
							<option value="1">Richiesta informazioni</option>
							<option value="2">Reso/Rimborso</option>
							<option value="3">Reclami/consigli</option>
							<option value="4">Altro</option>
						</select>
					</div>
					<div class="form-group">
						<label for="messaggio_contatto">Messaggio<span class="obbligatorio">*</span>:</label>
						<textarea class="form-control" id="messaggio_contatto" name="messaggio_contatto" rows="10" placeholder="Messaggio..." required></textarea>
					</div>
					<h3 id="res_contatto"></h3>
					<br style="display: none;"/>
					<button type="submit" class="btn btn-info">INVIA</button>
				</form>
				<br/>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function() {
					$("#form_contatto").submit(function(e){
						e.preventDefault()
						let serializeData = $(this).serialize()

						$.ajax({
							type: 'POST',
							url: <?php echo "'"._ROOT_DIR_."/file/pagine/ajax/r_inviaRichista.php'"?>,
							data: serializeData,
							dataType: "html",
							cache: false,
							complete: function(r, ts)
							{
								$("#res_contatto").next().show()

								if(r.responseText == "" && ts === "success")
								{
									$("#res_contatto").css("color", "green")
									$("#res_contatto").text("Inviato con successo")
									$("#form_contatto").trigger("reset")
								}
								else
								{
									$("#res_contatto").css("color", "red")
									$("#res_contatto").text(r.responseText)
								}
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						});
					})
				})
			</script>
		</div>
	</body>
</html>
