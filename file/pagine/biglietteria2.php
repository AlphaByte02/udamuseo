<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(!isset($_SESSION["acquisto"]))
		header("Location: ". _ROOT_DIR_ . "/file/pagine/eventi.php");

	$acquisto = unserialize($_SESSION["acquisto"]);
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Biglietteria</title>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="eventi";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<br/>
				<form id="formAggiuntaAggiunte" method="POST">
					<div class="card">
						<div class="card-body text-dark">
							<h4 class="card-text">Servizi aggiuntivi opzionali per "<?php echo $acquisto->getTitoloEvento(); ?>"</h4>
							<p class="card-text">Prezzo Stimato: &euro;<span class='prezzoS'>0.00</span></p>
							<button type="submit" class="btn btn-primary">Continua</button>
						</div>
					</div>
					<br/>
					<div class="container">
						<?php
							$db = new DB();

							$servizi = array();
							$res = $db->runQuery("SELECT * FROM servizi");
							if($res !== false && $res->num_rows > 0)
								while($row = $res->fetch_assoc())
									$servizi[] = $row;

							$db->closeConnection();

							$visualNum = 3;
							foreach ($acquisto->getKeyRiduzioni() as $idRid)
							{
								echo "
								<div class='row'>
									<h3 class='w-100'>{$acquisto->getDescrizioneRiduzione($idRid)}</h3>
								</div>";
								for ($i = 1; $i <= $acquisto->getNBigliettiRiduzione($idRid); $i++)
								{
									if(($i-1) % $visualNum == 0 && ($i-1) != 0)
									{
										echo "</div>";
									}
									if(($i-1) % $visualNum == 0)
										echo "<div class='row m-2'>";

									echo "
										<div class='col-lg-" . (12/$visualNum) . "'>
											<div class='card'>
												<div class='card-header'>
													<h5 class='card-title' style='color:black;'>#$i</h5>
												</div>
												<div class='card-body'>";
													foreach ($servizi as $rowS)
													{
														echo "
															<div class='custom-control custom-checkbox' style='text-align: left;'>
																<input type='checkbox' class='custom-control-input' style='text-align: left;' id='{$idRid}_$rowS[codice]_$i' name='{$idRid}_$rowS[codice]_$i' value='$rowS[prezzo]'>
																<label class='custom-control-label' for='{$idRid}_$rowS[codice]_$i' style='text-align: left;color:black;'>$rowS[descrizione] (&euro;$rowS[prezzo])</label>
															</div>
														";
													}
												echo "</div>
											</div>
										</div>
									";
								}
								echo "</div><br/>";
							}
						?>
					</div>
					<br/>
					<div class="card">
						<div class="card-body text-dark">
							<p class="card-text">Biglietti Totali: <?php echo $acquisto->getNAllBiglietti();?></p>
							<p class="card-text">Prezzo Stimato: &euro;<span class='prezzoS'>0.00</span></p>
							<button type="submit" class="btn btn-primary">Continua</button>
						</div>
					</div>
				</form>
				<br/>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
					// TODO: Mettere a posto questo
					var somma = parseFloat(<?php echo $acquisto->getPrezzoTotale() ?> + "")
					$(".prezzoS").text(zeroFill(roundDecimal(parseFloat(somma), 2), 2))

					$(".custom-control > input[type=checkbox]").change(function() {
						if ($(this).prop('checked'))
							somma += parseFloat($(this).val());
						else
							somma -= parseFloat($(this).val());

						$(".prezzoS").text(zeroFill(roundDecimal(parseFloat(somma), 2), 2));
					});

					$("#formAggiuntaAggiunte").submit(function(e)
					{
						e.preventDefault()

						let serializeData = $(this).serialize()

						$.ajax({
							type: 'POST',
							url: "ajax/r_aggiuntaServiziAcquisto.php",
							data: serializeData,
							dataType: "html",
							cache: false,
							complete: function(r, ts)
							{
								if(r.responseText == "" && ts === "success")
									location.href = "riepilogo.php"
								else
									console.log("Errore: " + r.responseText);
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						})
					})
				})
			</script>
		</div>
	</body>
</html>
