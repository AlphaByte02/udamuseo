<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(isset($_GET["id"]))
	{
		$db = new DB();
		$idEvento = $db->realEscapeString($_GET["id"]);

		$res = $db->runQuery("SELECT * FROM visite WHERE codice=$idEvento");
		if($res !== false && $res->num_rows == 1)
		{
			$evento = $res->fetch_assoc();
			if(!is_null($evento["data_fine"]))
				if((new DateTime($evento["data_fine"]))->format("md") < (new DateTime("now"))->format("md"))
					header("Location: ". _ROOT_DIR_ . "/file/pagine/eventi.php");
		}
		else
			header("Location: ". _ROOT_DIR_ . "/file/pagine/eventi.php");
	}
	else
		header("Location: ". _ROOT_DIR_ . "/file/pagine/eventi.php");
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Biglietteria</title>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="eventi";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title" style='color:black;'><?php echo $evento["titolo"]; ?></h5>
						<?php
							if(!is_null($evento["data_inizio"]) && !is_null($evento["data_fine"]))
							{
								$data_inizio = new DateTime($evento["data_inizio"]);
								$data_inizio = $data_inizio->format("d") . " " . getNameMonthIta($data_inizio->format("m")) . " " . $data_inizio->format("Y");
								$data_fine = new DateTime($evento["data_fine"]);
								$data_fine = $data_fine->format("d") . " " . getNameMonthIta($data_fine->format("m")) . " " . $data_fine->format("Y");

								echo "<p class='card-text' style='color:black;text-align:left;'>Periodo Evento: $data_inizio ➜ $data_fine</p><hr/>";
							}
						?>
						<form id="formQtaBiglietti" method="post">
							<div class="custom-control custom-checkbox" style="text-align:left;">
								<input type="checkbox" class="custom-control-input" id="viewRid">
								<label class="custom-control-label" style='color:black;' for="viewRid">Visualizza Riduzioni</label>
							</div>
							<hr/>
							<input type="hidden" name="idEvento" value="<?php echo $idEvento; ?>" readonly>
							<input type="hidden" name="titoloEvento" value="<?php echo $evento["titolo"]; ?>" readonly>
							<div class="container text-dark">
								<div class="row mb-2">
									<div class="col-md-2 my-auto">
										<h6 class="mb-0">Intero</h6>
									</div>
									<div class="col-md-10">
										<div class="input-group">
											<input type='hidden' id='sconto_1' value='0'>
											<input class="form-control rounded-left" type="number" name='riduzione_1' value="1" min='0' step="1" required>
											<div class="input-group-append biglietto">
												<span class='input-group-text'>&euro; <?php echo $evento["tariffa"] * (100-0) / 100; ?></span>
												<!--span class="input-group-text"><?php echo "-0%"; ?></span-->
											</div>
										</div>
									</div>
								</div>
								<div id='riduzioni' style='display:none;'>
									<?php
										$res = $db->runQuery("SELECT codice, descrizione, sconto FROM categorie WHERE codice > 1");
										if($res !== false && $res->num_rows > 0)
										{
											while($row = $res->fetch_assoc())
											{
												echo "
													<div class='row mb-2'>
														<div class='col-md-2 my-auto'>
															<h6 class='mb-0'>$row[descrizione]</h6>
														</div>
														<div class='col-md-10'>
															<div class='input-group'>
																<input type='hidden' id='sconto_$row[codice]' value='$row[sconto]'>
																<input class='form-control rounded-left' type='number' name='riduzione_$row[codice]' value='0' min='0' step='1' required>
																<div class='input-group-append biglietto'>
																	<span class='input-group-text'>&euro; " . ($evento["tariffa"] * (100-$row["sconto"]) / 100) . "</span>
																	<span class='input-group-text'>-$row[sconto]%</span>
																</div>
															</div>
														</div>
													</div>
												";
											}
										}
									?>
								</div>
							</div>
							<p class="card-text" style='color:black;text-align:right;'>Prezzo Stimato: &euro; <span id='prezzoS'>0.00</span></p>
							<input type="hidden" name="prezzo_base" value="<?php echo $evento["tariffa"]; ?>">
							<h4 style="color:red" id="error_text"></h4>
							<button id="button_prosegui" class="btn btn-primary">Prosegui</button>
						</form>
						<?php $db->closeConnection(); ?>
					</div>
				</div>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
			<script type="text/javascript">
				$(document).ready(function(){
					var prezzoIntero = parseFloat(<?php echo $evento["tariffa"]; ?> + "")
					var somma = 0

					var somme = {}
					$("#formQtaBiglietti input.form-control[type=number]").each(function(){
						somme[$(this).attr("name")] = 0
						for(let i = 0; i < $(this).val(); i++)
							addCosto(parseInt($(this).val()), parseFloat($(this).prev().val()), $(this).attr("name"))
					})

					$("#viewRid").click(function(){
						if($(this).prop("checked"))
							$(this).next().text("Nascondi Riduzioni")
						else
							$(this).next().text("Visualizza Riduzioni")

						$("#riduzioni").toggle($(this).prop("checked"))
					})

					$("#formQtaBiglietti").submit(function(e){
						e.preventDefault()

						var ok = true, s = 0
						$("#formQtaBiglietti input.form-control[type=number]").each(function(){
							ok &= ($(this).val() != "")
							s += parseInt($(this).val())
						})
						if(ok && s > 0)
						{
							let serializeData = $(this).serialize()

							$.ajax({
								type: 'POST',
								url: "ajax/r_inizioAcquisto.php",
								data: serializeData,
								dataType: "html",
								cache: false,
								complete: function(r, ts)
								{
									if(r.responseText == "" && ts === "success")
									{
										if($('#viewRid').prop("checked"))
											$('#viewRid').click()
										location.href = "biglietteria2.php"
									}
									else
										$("#error_text").text(r.responseText)
								},
								error: function() {
									console.log("Errore")
									alert("La richiesta non è andata a buon fine, riprovare")
								}
							})
						}
						else
						{
							$("#error_text").text(s != 0 ? "Completa correttamente tutti i campi" : "Deve essere comprato almeno un biglietto!")
						}
					})

					$("#formQtaBiglietti input.form-control[type=number]").on("input", function(){
						if($(this).val() != "")
						{
							let q = parseInt($(this).val())
							let sc = parseFloat($(this).prev().val())

							addCosto(q, sc, $(this).attr("name"))
						}
					})

					function addCosto(quantita, sconto, name)
					{
						while(quantita != parseInt(somme[name]))
						{
							somma = parseFloat(somma) + (quantita >= parseInt(somme[name]) ? parseFloat(prezzoIntero * (100 - sconto) / 100) : parseFloat(prezzoIntero * (100 - sconto) / 100 * -1))
							somme[name] += quantita >= parseInt(somme[name]) ? 1 : -1
						}

						$("#prezzoS").text(zeroFill(roundDecimal(parseFloat(somma), 2), 2))
					}
				})
			</script>
		</div>
	</body>
</html>
