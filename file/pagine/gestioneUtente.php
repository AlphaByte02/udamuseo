<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}

	if(isset($_SESSION["user"]))
		$user = unserialize($_SESSION["user"]);
	else
		header("Refresh: 3; url= " . _ROOT_DIR_ . "/");
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>Gestione utente</title>

		<style>
			hr {
				width: 100%;
				border: 0;
				height: 2px;
				background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.85), rgba(0, 0, 0, 0));
			}

			table, tr, th, td {
				margin-left: auto;
				margin-right: auto;

				vertical-align: middle !important;
				text-align: center;
				padding: 3px 3px;
				color: white;
			}

			#datipersonali {
				border: 2px solid gray;
				border-radius: 10px;
				padding: 5px 5px;
			}
		</style>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina = "utente";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<?php
					if(!isset($user)){
						echo "<h2>DEVI AVERE EFFETTUATO IL LOGIN PER ACCEDERE A QUESTA PAGINA!</h2></main>";
						include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php";
						die("</div></body></html>");
					}

					echo "<h1 class='cover-heading'>Ciao " . $user->getCognomeNome() . "</h1>";
				?>

				<div class="container mt-4">
					<div class="row">
						<div id="datipersonali" class="col-lg-6 h-100 mb-4">
							<div class="row"><div class="col-md-12 text-center"><h3>I Tuoi Dati</h3></div></div>
							<div class="container">
								<div class="row mb-2">
									<div class="col-md-12"><img class="rounded-pill" src='<?php echo "https://robohash.org/{$user->getCognomeNome()}.png"; ?>'></div>
								</div>
								<h5 id="text_error_modifica" style="color: red"></h5>
								<div id="origineDati">
									<div class="row mb-3">
										<div class="col-md-6 my-auto"><h5 style="display: inline-block; margin: 0">Cognome Nome:</h5></div>
										<div class="col-md-6 my-auto"><?php echo $user->getCognomeNome() ?></div>
									</div>
									<div class="row mb-3">
										<div class="col-md-6"><h5 style="display: inline-block; margin: 0">Email:</h5></div>
										<div class="col-md-6 my-auto"><?php echo $user->getEmail() ?></div>
									</div>
									<div class="row mb-3">
										<div class="col-md-12 my-auto"><button type="button" class="btn btn-primary button_modificaDati">Modifica i tuoi dati</button></div>
									</div>
								</div>
								<div id="modificaDati" style="display: none">
									<div class="row mb-2"><div class="col-md-12"><h4 class="mb-0">Modifica Cognome Nome</h4></div></div>
									<div class="row mb-3">
										<div class="col-md-4 my-auto mx-auto"><input type="text" class="form-control mb-1" name="modificaCognome" id="modificaCognome" placeholder="Cognome..."/></div>
										<div class="col-md-4 my-auto mx-auto"><input type="text" class="form-control mb-1" name="modificaNome" id="modificaNome" placeholder="Nome..."/></div>
										<div class="col-md-4 my-auto mx-auto"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalCambioCognomeNome" data-backdrop="static" data-keyboard="false" id="button_CognomeNome">Modifica Cognome Nome</button></div>
									</div>
									<div class="row mb-2"><div class="col-md-12"><h4 class="mb-0">Modifica Email</h4></div></div>
									<div class="row mb-3">
										<div class="col-md-6 my-auto mx-auto"><input type="email" class="form-control mb-1" name="modificaEmail" id="modificaEmail" placeholder="Email..."/></div>
										<div class="col-md-6 my-auto mx-auto"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalCambioEmail" data-backdrop="static" data-keyboard="false" id="button_Email">Modifica Email</button></div>
									</div>
									<div class="row mb-2"><div class="col-md-12"><h4 class="mb-0">Modifica Password</h4></div></div>
									<div class="row mb-3">
										<div class="container">
											<div class="row mb-1">
												<div class="col-md-6 my-auto"><input type="password" class="form-control" name="modificaOldPassword" id="modificaOldPassword" placeholder="Vecchia Password..."/></div>
											</div>
											<div class="row mb-1">
												<div class="col-md-6 my-auto"><input type="password" class="form-control" name="modificaNewPassword" id="modificaNewPassword" placeholder="Nuova Password..."/></div>
											</div>
											<div class="row">
												<div class="col-md-6 my-auto"><input type="password" class="form-control mb-1" name="modificaNewPasswordConferma" id="modificaNewPasswordConferma" placeholder="Conferma Nuova Password..."/></div>
												<div class="col-md-6 my-auto"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalCambioPassword" data-backdrop="static" data-keyboard="false">Modifica Password</button></div>
											</div>
										</div>
									</div>
									<div class="row mb-2"><div class="col-md-12"><h4 class="mb-0">Elimina il tuo profilo</h4></div></div>
									<div class="row mb-3">
										<div class="col-md-12 my-auto"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalEliminaProfilo" data-backdrop="static" data-keyboard="false">Elimina il tuo profilo</button></div>
									</div>
									<hr/>
									<div class="row mb-3">
										<div class="col-md-12 my-auto"><button type="button" class="btn btn-primary button_modificaDati">Torna Indietro</button></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<?php
								$db = new DB();
								$res = $db->runQuery("SELECT data_emissione,titolo,SUM((tariffa * (100-c.sconto) / 100) + IFNULL((SELECT SUM(prezzo) FROM accessori JOIN servizi ON codice_servizio=codice WHERE accessori.id_biglietto=b.id_biglietto GROUP BY id_biglietto), 0)) AS 'costo', c.descrizione, COUNT(*) AS 'nbiglietti' FROM biglietti AS b JOIN visite AS v ON codice_visita=v.codice JOIN categorie AS c ON codice_categoria=c.codice WHERE b.id_utente={$user->getId()} GROUP BY b.data_emissione,v.titolo,c.descrizione ORDER BY b.data_emissione DESC,c.descrizione ASC");
								if($res !== false && $res->num_rows > 0)
								{
									echo "<h3>Tutti i biglietti che hai acquistato</h3>";
									echo "<div class='table-responsive'>";
										echo "<table class='table table-dark table-bordered table-striped'>";
											echo "<thead>";
												echo "<tr>";
													echo "<th>Categoria</th>";
													echo "<th>Titolo Evento</th>";
													echo "<th>Data di Emissione</th>";
													echo "<th>Costo (€)</th>";
												echo "</tr>";
											echo "</thead>";
											echo "<tbody>";
												while($row = $res->fetch_assoc())
												{
													echo "<tr>";
														echo "<td>$row[descrizione]" . ($row["nbiglietti"] > 1 ? " (x$row[nbiglietti])" : "") . "</td>";
														echo "<td>$row[titolo]</td>";
														echo "<td>" . (new DateTime($row["data_emissione"]))->format("d/m/Y") . "</td>";
														echo "<td>" . (sprintf("%04.2f", $row["costo"])) . "</td>";
													echo "</tr>";
												}
											echo "</tbody>";
										echo "</table>";
									echo "</div>";
								}
								else
									echo "<h3>Non hai acquistato ancora nessun biglietto<br/>Fallo ora <a class='link' href='" . _ROOT_DIR_ . "/file/pagine/eventi.php" . "'>qui!</a></h3>";

								$db->closeConnection();
							?>
						</div>
					</div>
				</div>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
			<script>
				$(document).ready(function(){
					$(".button_modificaDati").click(function(){
						$("#origineDati").toggle()
						$("#modificaDati").toggle()
						if($("#text_error_modifica").text() != "")
							$("#text_error_modifica").text("")
					})

					$("#button_CognomeNome").click(function(){
						$("#modalVisCognome").text($("#modificaCognome").val())
						$("#modalVisNome").text($("#modificaNome").val())
					})

					$("#button_Email").click(function(){
						$("#modalVisEmail").text($("#modificaEmail").val())
					})

					$("#button_modificaDatiCognomeNome").click(function(){
						$.ajax({
							type: 'POST',
							url: "ajax/r_modificaCognomeNome.php",
							data: { cognome: $.trim($("#modificaCognome").val()), nome: $.trim($("#modificaNome").val()), password: $("#valida_passwordCognomeNome").val() },
							dataType: "html",
							cache: false,
							complete: function(r, ts){
								if(r.responseText == "" && ts === "success")
									location.reload()
								else
								{
									$("#valida_passwordCognomeNome").val("")
									$("#text_error_modifica").text(r.responseText)
								}
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						});
					});

					$("#button_modificaDatiEmail").click(function(){
						$.ajax({
							type: 'POST',
							url: "ajax/r_modificaMail.php",
							data: { email: $("#modificaEmail").val(), password: $("#valida_passwordEmail").val() },
							dataType: "html",
							cache: false,
							complete: function(r, ts){
								if(r.responseText == "" && ts === "success")
									location.reload()
								else
								{
									$("#valida_passwordEmail").val("")
									$("#text_error_modifica").text(r.responseText)
								}
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						});
					});

					$("#button_modificaDatiPassword").click(function(){
						$.ajax({
							type: 'POST',
							url: "ajax/r_modificaPassword.php",
							data: { oldpassword: $.trim($("#modificaOldPassword").val()), newpassword: $.trim($("#modificaNewPassword").val()), conferma_newpassword: $.trim($("#modificaNewPasswordConferma").val()) },
							dataType: "html",
							cache: false,
							complete: function(r, ts){
								if(r.responseText == "" && ts === "success")
								{
									$(".button_modificaDati").get(1).click()
									$("#modificaOldPassword").val("")
									$("#modificaNewPassword").val("")
									$("#modificaNewPasswordConferma").val("")
								}
								else
									$("#text_error_modifica").text(r.responseText)
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						});
					});

					$("#button_eliminaProfilo").click(function(){
						$.ajax({
							type: 'POST',
							url: "ajax/r_eliminaProfilo.php",
							data: { password: $("#valida_passwordEliminaProfilo").val() },
							dataType: "html",
							cache: false,
							complete: function(r, ts){
								if(r.responseText == "" && ts === "success")
									location.href = "<?php echo _ROOT_DIR_ . "/"; ?>"
								else
								{
									$("#valida_passwordEliminaProfilo").val("")
									$("#text_error_modifica").text(r.responseText)
								}
							},
							error: function() {
								console.log("Errore")
								alert("La richiesta non è andata a buon fine, riprovare")
							}
						});
					});
				})
			</script>

			<!-- Modal cambio CognomeNome -->
			<div class="modal fade" id="modalCambioCognomeNome">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Conferma modifica</h4>
						</div>
						<div class="modal-body">
							<h5>Sicuro di voler modificare i seguenti dati?</h5>
							Cognome: <span id="modalVisCognome">-</span><br/>
							Nome: <span id="modalVisNome">-</span><br/><br/>
							<label for="valida_passwordCognomeNome">Conferma Password<span class="obbligatorio">*</span>:</label>
							<input type="password" class="form-control mx-auto" style="max-width: 75%" name="valida_passwordCognomeNome" id="valida_passwordCognomeNome"/>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#valida_passwordCognomeNome').val('')">Annulla</button>
							<button type="button" class="btn btn-success" data-dismiss="modal" id="button_modificaDatiCognomeNome" disabled>Conferma</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal cambio Email -->
			<div class="modal fade" id="modalCambioEmail">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Conferma modifica</h4>
						</div>
						<div class="modal-body">
							<h5>Sicuro di voler cambiare la email in: <span id="modalVisEmail">-</span></h5>
							<label for="valida_passwordEmail">Conferma Password<span class="obbligatorio">*</span>:</label>
							<input type="password" class="form-control mx-auto" style="max-width: 75%" name="valida_passwordEmail" id="valida_passwordEmail"/>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#valida_passwordEmail').val('')">Annulla</button>
							<button type="button" class="btn btn-success" data-dismiss="modal" id="button_modificaDatiEmail" disabled>Conferma</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal cambio Password -->
			<div class="modal fade" id="modalCambioPassword">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Conferma Modifica</h4>
						</div>
						<div class="modal-body">
							<h5>Sicuro di voler modificare la password?</h5>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Annulla</button>
							<button type="button" class="btn btn-success" data-dismiss="modal" id="button_modificaDatiPassword">Conferma</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal elimina profilo -->
			<div class="modal fade" id="modalEliminaProfilo">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Conferma Elimina</h4>
						</div>
						<div class="modal-body">
							<h5>Sicuro di voler eliminare il profilo?</h5>
							<h6 class="text-warning"><strong>ATTENZIONE!</strong> QUESTA AZIONE È IRREVERSIBILE!</h6>
							<label for="valida_passwordEliminaProfilo">Conferma Password<span class="obbligatorio">*</span>:</label>
							<input type="password" class="form-control mx-auto" style="max-width: 75%" name="valida_passwordEliminaProfilo" id="valida_passwordEliminaProfilo"/>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#valida_passwordEliminaProfilo').val('')">Annulla</button>
							<button type="button" class="btn btn-success" data-dismiss="modal" id="button_eliminaProfilo" disabled>Conferma</button>
						</div>
					</div>
				</div>
			</div>
			<script>
				$(document).ready(function(){
					$("#valida_passwordEmail").keyup(function(){
						$("#button_modificaDatiEmail").prop("disabled", $(this).val() === "")
					})
					$("#valida_passwordCognomeNome").keyup(function(){
						$("#button_modificaDatiCognomeNome").prop("disabled", $(this).val() === "")
					})
					$("#valida_passwordEliminaProfilo").keyup(function(){
						$("#button_eliminaProfilo").prop("disabled", $(this).val() === "")
					})
					$(".modal-footer > button.btn-danger").click(function(){
						$('#valida_passwordEmail').val('')
						$("#button_modificaDatiEmail").prop("disabled", true)

						$('#valida_passwordCognomeNome').val('')
						$("#button_modificaDatiCognomeNome").prop("disabled", true)

						$('#valida_passwordEliminaProfilo').val('')
						$("#button_eliminaProfilo").prop("disabled", true)
					})
				})
			</script>
		</div>
	</body>
</html>
