<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		
		<!-- SEO -->
		<title>Termini e Privacy Policy | Museo Storico del Severi a Padova</title>
		<meta name="description" content="Termini e Privacy Policy per il sito del Museo Storico del Severi a Padova">
		<meta property="og:title" content="Termini e Privacy Policy | Museo Storico del Severi a Padova">
		<meta property="og:image" content="<?php echo _ROOT_DIR_ . '/file/struct/immagini/favicon.png';?>">
		<meta property="og:site_name" content="Severi Museo">
	
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="about";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";
			?>
			<main role="main" class="pt-2">
				<br/>

					<h1>INFORMATIVA PRIVACY AI SENSI DELL’ART. 13 DEL REGOLAMENTO UE 2016/679</h1>
					<p>(Informativa aggiornata al 18 maggio 2019)</p>
					<p style="text-align:justify;">
					Ai sensi dell’articolo 13 del Regolamento (UE) 2016/679 La informiamo che i dati personali acquisiti saranno trattati, anche in via automatizzata, da Severi Museo con sede a Padova in Via Luigi Pettinati 46, Tel 049 865 8111, mail: info@udamuseo.garbo.tech , per l’invio di materiale informativo o comunicazioni legate alle diverse attività di Severi Museo .<br/>
					<p/>
					<h2 style="text-align:left;">Il “Titolare del trattamento”</h2>
					<p style="text-align:justify;">
					Severi Museo di Padova, ai sensi dell’art. 24 del Regolamento UE 2016/679 relativo alla protezione delle persone fisiche con riguardo al trattamento dei dati personali, nonché alla libera circolazione di tali dati (di seguito: “Regolamento UE”), è Titolare del trattamento dei dati personali ed in attuazione dell’art. 13 del Regolamento UE (“Informazioni da fornire qualora i dati personali siano raccolti presso l’interessato”) e La informa che i dati personali acquisiti formano oggetto di trattamento nel rispetto della normativa sopra richiamata.<br/>
					<br/>
					In relazione ai suddetti trattamenti il Titolare fornisce, tra l’altro, le seguenti informazioni:<br/>
					– per “dato personale” (ex 4 numero 1 del Regolamento UE 2016/679), si intende qualsiasi informazione riguardante una persona fisica identificata o identificabile («interessato»); si considera identificabile la persona fisica che può essere identificata, direttamente o indirettamente, con particolare riferimento a un identificativo come il nome, un numero di identificazione, dati relativi all’ubicazione, un identificativo online o a uno o più elementi caratteristici della sua identità fisica, fisiologica, genetica, psichica, economica, culturale o sociale;<br/>
					– per “trattamento” (ex 4 numero 2 del Regolamento UE 2016/679), si intende qualsiasi operazione o insieme di operazioni, compiute con o senza l’ausilio di processi automatizzati e applicate a dati personali o insiemi di dati personali, come la raccolta, la registrazione, l’organizzazione, la strutturazione, la conservazione, l’adattamento o la modifica, l’estrazione, la consultazione, l’uso, la comunicazione mediante trasmissione, diffusione o qualsiasi altra forma di messa a disposizione, il raffronto o l’interconnessione, la limitazione, la cancellazione o la distruzione. Tale trattamento deve essere improntato ai principi di correttezza, liceità, trasparenza, tutelando la Sua riservatezza e i Suoi diritti.<br/>
					<br/></p>
					<h2 style="text-align:left;">Titolare del trattamento</h2>
					<p style="text-align:justify;">
					Ragione Sociale: ITI Severi Padova<br/>
					Indirizzo sede legale: Via Luigi Pettinati, 46, 35129 Padova PD<br/>
					Dati contatto telefonico: 049 865 8111<br/>
					Dati contatto email: info@udamuseo.garbo.tech<br/>
<br/>
					Data Protection Officer (DPO)<br/>
					Nome e Cognome: Francesco Garbo<br/>
					Domicilio per l’incarico: Via Luigi Pettinati, 46, 35129 Padova PD<br/>
					Dati contatto email: francesco@garbo.tech<br/>
<br/></p>
					<h2 style="text-align:left;">Dati personali raccolti</h2>
					<p style="text-align:justify;">
					I dati personali raccolti sono inerenti essenzialmente a:<br/>
					Dati identificativi (nome e cognome, indirizzo e-mail). Gli stessi sono forniti dall’interessato (nella sua accezione più ampia) direttamente.<br/>
<br/></p>
					<h2 style="text-align:left;">Finalità del trattamento</h2>
					<p style="text-align:justify;">
					Le finalità del trattamento dei dati personali sono le seguenti:<br/>
					– attività di marketing e pubblicità attraverso l’invio, anche via mail, di materiale promozionale e di informazioni relative alle diverse attività di Severi Museo<br/>
					– invito e partecipazione a corsi di formazione, convegni, seminari, eventi, manifestazioni, fiere, etc..<br/>
<br/></p>
					<h2 style="text-align:left;">Periodo di Conservazione</h2>
					<p style="text-align:justify;">
					Per un periodo di 120 mesi dal rilascio del consenso, salvo disdetta.<br/>
<br/>
</p>
					<h2 style="text-align:left;">Diritti dell’interessato</h2>
					<p style="text-align:justify;">
					L’interessato, in relazione ai dati personali oggetto della presente informativa, ha la facoltà di esercitare i diritti previsti dal Regolamento UE di seguito riportati:<br/>
					– diritto di accesso dell’interessato [art. 15 del Regolamento UE] (la possibilità di essere informato sui trattamenti effettuati sui propri Dati Personali ed eventualmente riceverne copia);<br/>
					– diritto di rettifica dei propri Dati Personali [art. 16 del Regolamento UE] (l’interessato ha diritto alla rettifica dei dati personali inesatti che lo riguardano);<br/>
					– diritto alla cancellazione dei propri Dati Personali senza ingiustificato ritardo (“diritto all’oblio”) [art. 17 del Regolamento UE] (l’interessato ha, così come avrà, diritto alla cancellazione dei propri dati);<br/>
					– diritto di limitazione di trattamento dei propri Dati Personali nei casi previsti dall’art. 18 del Regolamento UE, tra cui nel caso di trattamenti illeciti o contestazione dell’esattezza dei Dati Personali da parte dell’interessato [art. 18 del Regolamento UE];<br/>
					– diritto alla portabilità dei dati [art. 20 del Regolamento UE], l’interessato potrà richiedere in formato strutturato i propri Dati Personali al fine di trasmetterli ad altro titolare, nei casi previsti dal medesimo articolo;<br/>
					– diritto di opposizione al trattamento dei propri Dati Personali [art. 21 del Regolamento UE] (l’interessato ha, così come avrà, diritto alla opposizione del trattamento dei propri dati personali);<br/>
					– diritto di non essere sottoposto a processi decisionali automatizzati, [art. 22 del Regolamento UE] (l’interessato ha, così come avrà, diritto a non essere sottoposto ad una decisione basata unicamente sul trattamento automatizzato).<br/>
<br/>
					Ulteriori informazioni circa i diritti dell’interessato potranno ottenersi sul sito www.garanteprivacy.it ovvero chiedendo al Titolare estratto integrale degli articoli sopra richiamati.<br/>
<br/>
					I suddetti diritti possono essere esercitati secondo quanto stabilito dal Regolamento inviando una email a info@udamuseo.garbo.tech<br/>
<br/>
					Severi Museo, in ossequio all’art. 19 del Regolamento UE, procedono a informare i destinatari cui sono stati comunicati i dati personali, le eventuali rettifiche, cancellazioni o limitazioni del trattamento richieste, ove ciò sia possibile.<br/>
<br/>
					Con riferimento alle finalità sopra indicate, l’interessato ha la facoltà di procedere, in ogni momento, alla revoca del consenso del trattamento dei dati identificativi e anagrafici inviando una email a info@udamuseo.garbo.tech<br/>
<br/>
					Ai sensi dell’art. 7 del Regolamento UE, la revoca del consenso non comporta pregiudizio sulla liceità del trattamento basata sul consenso effettuato prima dell’avvenuta revoca.<br/>
<br/></p>
					<h2 style="text-align:left;">Diritto di porre Reclamo</h2>
					<p style="text-align:justify;">
					L’interessato, qualora ritenga che i propri diritti siano stati compromessi, ha diritto di proporre reclamo all’Autorità Garante per la protezione dei dati personali, secondo le modalità indicate dalla stessa Autorità al seguente indirizzo internet:<br/>
					www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/4535524.<br/>
<br/></p>
					<h2 style="text-align:left;">Possibili conseguenze del mancato conferimento dei dati</h2>
					<p style="text-align:justify;">
					Il consenso al trattamento dei dati per le finalità di cui sopra è libero e facoltativo. Tuttavia l’eventuale rifiuto comporta l’impossibilità da parte del titolare di dare seguito alla suddetta finalità.<br/>
					Severi Museo non si avvale di alcun processo decisionale automatizzato.<br/>
<br/></p>
					<h2 style="text-align:left;">Modalità del trattamento</h2>
					<p style="text-align:justify;">
					I dati personali verranno trattati in forma cartacea, informatizzata e telematica ed inseriti nelle pertinenti banche dati (clienti, utenti, ecc.) cui potranno accedere, e quindi venirne a conoscenza, gli addetti espressamente designati dal Titolare quali Responsabili ed Incaricati del trattamento dei dati personali, che potranno effettuare operazioni di consultazione, utilizzo, elaborazione, raffronto ed ogni altra opportuna operazione anche automatizzata nel rispetto delle disposizioni di legge necessarie a garantire, tra l’altro, la riservatezza e la sicurezza dei dati nonché l’esattezza, l’aggiornamento e la pertinenza dei dati rispetto alle finalità dichiarate.<br/>
<br/></p>
					<h2 style="text-align:left;">Modifiche e aggiornamenti</h2>
					<p style="text-align:justify;">
					La presente informativa è valida dalla data indicata nella sua intestazione.<br/>
					Severi Museo potrebbe inoltre apportare modifiche e/o integrazioni a detta informativa anche quale conseguenza di eventuali e successive modifiche e/o integrazioni normative.  L’interessato potrà visionare il testo dell’informativa costantemente aggiornata sul sito internet https://udamuseo.garbo.tech<br/>
				
				</p>
				
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
		</div>
	</body>
</html>
