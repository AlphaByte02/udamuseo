<?php
	require_once "../../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$db = new DB();

		$idEvento = $db->realEscapeString(trim(@$_POST["idEvento"])) ?? NULL;
		$prezzoBase = $db->realEscapeString(trim(@$_POST["prezzo_base"])) ?? NULL;

		if(!empty($idEvento) && !empty($prezzoBase))
		{
			$riduzioni = array();
			$res = $db->runQuery("SELECT codice, descrizione, sconto FROM categorie");
			if($res !== false && $res->num_rows > 0)
			{
				while($row = $res->fetch_assoc())
				{
					if(isset($_POST["riduzione_$row[codice]"]) && $_POST["riduzione_$row[codice]"] > 0)
						$riduzioni[$row["codice"]] = array("sconto" => $row["sconto"], "q" => $db->realEscapeString($_POST["riduzione_$row[codice]"]), "d" => $row["descrizione"]);
				}
			}
			else
				echo "Errore";

			if(count($riduzioni) > 0)
			{
				$acquisto = new Acquisto($idEvento, $prezzoBase, $db->realEscapeString(@$_POST["titoloEvento"]));

				foreach ($riduzioni as $id => $v)
					$acquisto->addBiglietto($id, $v["sconto"], $v["d"], $v["q"]);

				$_SESSION["acquisto"] = serialize($acquisto);
			}
			else
				echo "Nessuna riduzione individuata";
		}
		else
			echo "Errore";

		$db->closeConnection();
	}
?>
