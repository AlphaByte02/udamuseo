<?php
	require_once "../../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$oldPassword = trim(@$_POST["oldpassword"]) ?? NULL;
		$newPassword = trim(@$_POST["newpassword"]) ?? NULL;
		$conferma_newPassword = trim(@$_POST["conferma_newpassword"]) ?? NULL;

		if(isset($_SESSION["user"]))
		{
			if(!empty($oldPassword) && !empty($newPassword) && !empty($conferma_newPassword))
			{
				if($newPassword === $conferma_newPassword)
				{
					$user = unserialize($_SESSION["user"]);
					try
					{
						$user->setNewPassword($oldPassword, $newPassword);
					}
					catch (\Exception $e)
					{
						echo $e->getMessage();// echo "Errore nella modifica della password!";
					}
				}
				else
					echo "Le due password devo coincidere!";
			}
			else
				echo "Completa correttamente i campi per il cambio password";
		}
		else
			echo "Dovresti essere loggato, come fai ad essere qui?!";
	}
?>
