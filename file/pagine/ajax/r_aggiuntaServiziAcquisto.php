<?php
	require_once "../../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_SESSION["acquisto"]))
	{
		$db = new DB();

		$servizi = array();
		$res = $db->runQuery("SELECT * FROM servizi");
		if($res !== false && $res->num_rows > 0)
			while($row = $res->fetch_assoc())
				$servizi[] = $row;

		$db->closeConnection();

		$acquisto = unserialize($_SESSION["acquisto"]);

		foreach ($acquisto->getKeyRiduzioni() as $idRid)
		{
			$acquisto->removeAllServiziRiduzione($idRid);

			foreach ($servizi as $serv)
			{
				for ($i = 1; $i <= $acquisto->getNBigliettiRiduzione($idRid); $i++)
					if(isset($_POST["{$idRid}_$serv[codice]_$i"]))
						$acquisto->addServizio($idRid, $serv["codice"], $serv["prezzo"], $serv["descrizione"], ($i-1));
			}
		}

		$_SESSION["acquisto"] = serialize($acquisto);
	}
?>
