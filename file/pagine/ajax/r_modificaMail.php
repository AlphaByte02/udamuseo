<?php
	require_once "../../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$email = trim(@$_POST["email"]) ?? NULL;
		$password = trim(@$_POST["password"]) ?? NULL;

		if(isset($_SESSION["user"]))
		{
			if(!empty($email) && !empty($password))
			{
				$user = unserialize($_SESSION["user"]);
				if($user->checkPassword($password))
				{
					try
					{
						$user->setEmail($email, $password);
						$_SESSION["user"] = serialize($user);
					}
					catch (\Exception $e)
					{
						echo $e->getMessage();// echo "Errore nella modifica dell'email!";
					}
				}
				else
					echo "La password inserita non è corretta";
			}
			else
				echo "Completa correttamente il campo dell'email";
		}
		else
			echo "Dovresti essere loggato, come fai ad essere qui?!";
	}
?>
