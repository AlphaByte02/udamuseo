<?php
	require_once "../../struct/include/functions.php";

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$db = new DB();

		$email = $db->realEscapeString(@$_POST['email_contatto']) ?? NULL;
		$oggetto = $db->realEscapeString(@$_POST['oggetto_contatto']) ?? NULL;
		$msg = $db->realEscapeString(@$_POST['messaggio_contatto']) ?? NULL;

		$res_invio = "";
		if(!empty($email) && !empty($oggetto) && !empty($msg))
		{
			$res = $db->runQuery("SELECT * FROM email WHERE email='$email'");
			if ($res !== false)
			{
				if($res->num_rows == 1)
				{
					$idEmail = $res->fetch_assoc()["cod_email"];
					if($db->runQuery("INSERT INTO richieste VALUES (NULL, $idEmail, $oggetto, '$msg', DEFAULT)") === false)
						$res_invio = "C'è stato qualche errore!";
				}
				else if($res->num_rows == 0)
				{
					if($db->runQuery("INSERT INTO email VALUES (NULL, '$email')") !== false)
					{
						$idEmail = $db->getInsertId();
						if($db->runQuery("INSERT INTO richieste VALUES (NULL, $idEmail, $oggetto, '$msg', DEFAULT)") === false)
							$res_invio = "C'è stato qualche errore!";
					}
					else
						$res_invio = "C'è stato qualche errore!";
				}
			}
			else
				$res_invio = "C'è stato qualche errore!";
		}
		else
			$res_invio = "Compila tutti i campi correttamente!";

		$db->closeConnection();
		echo $res_invio;
	}
?>
