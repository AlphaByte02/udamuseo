<?php
	require_once "../../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$cognome = trim(@$_POST["cognome"]) ?? NULL;
		$nome = trim(@$_POST["nome"]) ?? NULL;
		$password = trim(@$_POST["password"]) ?? NULL;

		if(isset($_SESSION["user"]))
		{
			if(!empty($cognome) && !empty($nome) && !empty($password))
			{
				$user = unserialize($_SESSION["user"]);
				if($user->checkPassword($password))
				{
					try
					{
						$user->setCognomeNome($cognome . " " . $nome);
						$_SESSION["user"] = serialize($user);
					}
					catch (\Exception $e)
					{
						echo $e->getMessage(); //echo "Errore nella modifica del cognome nome!";
					}
				}
				else
					echo "La password inserita non è corretta";
			}
			else
				echo "Completa correttamente tutti i campi del cognome e nome";
		}
		else
			echo "Dovresti essere loggato, come fai ad essere qui?!";
	}
?>
