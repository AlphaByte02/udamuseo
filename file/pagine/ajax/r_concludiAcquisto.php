<?php
	require_once "../../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_SESSION["acquisto"]))
	{
		$db = new DB();

		$acquisto = unserialize($_SESSION["acquisto"]);

		if(isset($_SESSION["idBiglietti"]))
			unset($_SESSION["idBiglietti"]);

		$userId = isset($_SESSION["user"]) ? (unserialize($_SESSION["user"]))->getID() : "NULL";

		$queryBiglietti = "INSERT INTO biglietti VALUES ";
		$queryServizi = "INSERT INTO accessori VALUES ";

		$res = $db->runQuery("SELECT id_biglietto FROM bigletti");
		$idBigletti = array();
		if ($res !== false && $res->num_rows > 0)
			while($id = $res->fetch_assoc())
				$idBigletti[] = $id["id_biglietto"];

		$idBiglietti = array();
		foreach ($acquisto->getBiglietti() as $idRid => $rid)
		{
			foreach ($acquisto->getBigliettiRiduzione($idRid) as $b)
			{
				if(endsWith(")", $queryBiglietti))
					$queryBiglietti .= ",";

				$rand = 0;
				do {
					$rand = mt_rand(1, mt_getrandmax());
				} while(in_array($rand, $idBigletti) || $rand == 0);
				$queryBiglietti .= "($rand, CURRENT_DATE(), {$acquisto->getIdEvento()}, $userId, $idRid)";
				$idBiglietti[] = $rand;

				if(!is_null($b->getServizi()))
				{
					foreach ($b->getServizi() as $idServ => $serv)
					{
						if(endsWith(")", $queryServizi))
							$queryServizi .= ",";

						$queryServizi .= "($rand, $idServ)";
					}
				}
			}
		}

		$error = "";
		if(endsWith(")", $queryBiglietti))
		{
			$db->beginTransaction();

			if($db->runQuery($queryBiglietti) === false)
				$error = "Errore nell'inserimento dei biglietti";

			if(empty($error) && endsWith(")", $queryServizi))
			{
				if($db->runQuery($queryServizi) === false)
					$error = "Errore nell'inserimento dei servizi";
			}
		}
		else
			$error = "Error";

		$db->endTransaction(empty($error));
		if(empty($error))
		{
			unset($_SESSION["acquisto"]);
			$_SESSION["idBiglietti"] = $idBiglietti;
		}
		else
			echo $error;

		$db->closeConnection();
	}
	else
		echo "Ci sono stati degli errori";
?>
