<?php
	require_once "../../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$password = trim(@$_POST["password"]) ?? NULL;

		if(isset($_SESSION["user"]))
		{
			if(!empty($password))
			{
				$user = unserialize($_SESSION["user"]);
				if($user->checkPassword($password))
				{
					try
					{
						$user->eliminaProfilo();
						unset($_SESSION["user"]);
					}
					catch (\Exception $e)
					{
						echo $e->getMessage();// echo "Errore nella modifica dell'email!";
					}
				}
				else
					echo "La password inserita non è corretta";
			}
			else
				echo "Inserisci la password per la validazione dell'utente";
		}
		else
			echo "Dovresti essere loggato, come fai ad essere qui?!";
	}
?>
