<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>

		<!-- SEO -->
		<title>Eventi Severi Museo | Museo Storico del Severi a Padova</title>
		<meta name="description" content="Tutti gli Eventi e Visite del Museo Storico del Severi situato a Padova">
		<meta property="og:title" content="Eventi - Museo Storico del Severi a Padova">
		<meta property="og:image" content="<?php echo _ROOT_DIR_ . '/file/struct/immagini/favicon.png';?>">
		<meta property="og:site_name" content="Severi Museo">

		<style media="screen">
			.mybtn {
				display: inline-block;

				font-weight: 400;
				text-align: center;
				vertical-align: middle;

				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;

				border: 1px solid transparent;
				padding: 2rem;
				font-size: 4vh;
				line-height: 1.5;
				border-radius: .25rem;

				margin-top: 10vh;
				margin-bottom: 10vh;
			}

			.mybtn:hover{
				text-decoration: none;
			}

			.myrow {
				margin: 15px 5px !important;
				border:2px solid gray;
				padding: 1px 5px;
				border-radius: 8px;

				min-height: 15vh;
			}

			.myrow:hover{
				border-color: #17a2b8;
				background-color: #17a2b8 !important;
			}

			.riga{
				display: block;
				margin-top: 0.5em;
				margin-bottom: 0.5em;
				margin-left: 20%;
				margin-right:20%;
				border-style: inset;
				border-width: 1px;
				border-top: 2px solid black;
			}

			a:hover {
				text-decoration: none;
			}

			@media only screen and (min-width: 768px) {
				.dateContainer {
					border-right: 2px solid white;
				}
			}

			@media only screen and (max-width: 767px) {
				.dateContainer {
					padding-bottom: 5px !important;
					margin-bottom: 20px !important;
					border-bottom: 2px solid white;
				}
			}
		</style>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="eventi";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";

				$db = new DB();

				if(isset($_GET['filtro']))
				{
					$filtro = $_GET['filtro'];

					if($filtro == "eventi")
						$res = $db->runQuery("SELECT codice,titolo,descrizione,data_inizio FROM visite WHERE data_fine >= DATE(now()) ORDER BY data_inizio DESC");
					elseif($filtro == "visite")
						$res = $db->runQuery("SELECT codice,titolo,descrizione FROM visite WHERE data_inizio IS NULL AND data_fine IS NULL");
				}

				$db->closeConnection();
			?>
			<main role="main" class="pt-2">
				<h1 class="black">Tutti gli Eventi e Visite del Museo Storico del Severi a Padova</h1>
				<?php
					if(isset($filtro))
					{
						if($res !== false && $res->num_rows > 0)
						{
							if($filtro == "eventi")
							{
								echo "<h4 class='black'>Passa alle:<a class='btn ml-2 btn-secondary transition-15s-easeio' role='button' href='eventi.php?filtro=visite'>Visite</a></h4>";
								echo "<hr class='riga'/>";
								echo "<br/>";
								echo "<h1 class='black'>Eventi Disponibili</h1>";
							}
							elseif($filtro == "visite")
							{
								echo "<h4 class='black'>Passa agli:<a class='btn ml-2 btn-secondary transition-15s-easeio' role='button' href='eventi.php?filtro=eventi'>Eventi</a></h4>";
								echo "<hr class='riga'/>";
								echo "<br/>";
								echo "<h1 class='black'>Visite Disponibili</h1>";
							}
							echo "<br/><br/>";

							echo "<div class='container'>";
								while($evento = $res->fetch_assoc())
								{
									$link = _ROOT_DIR_ . "/file/pagine/biglietteria.php?id=".$evento["codice"];

									echo "
										<a href='$link'>
											<div class='row bg-secondary myrow transition-15s-easeio'>";
											if($filtro == "eventi")
											{
												$date = new DateTime($evento["data_inizio"]);
												$data_inizioG = $date->format("d");
												$data_inizioM = strtoupper(substr(getNameMonthIta($date->format("m")), 0, 3));
												echo "
													<div class='col-md-2 my-auto dateContainer'>
														<h1>$data_inizioG</h1>
														<h2>$data_inizioM</h2>
													</div>";
											}
											echo "
												<div class='col-md my-auto'>";
													echo "<h1>$evento[titolo]</h1>";
													echo !is_null($evento["descrizione"]) ? "<p>$evento[descrizione]</p>" : "<p>Nessuna Descrizione Disponibile</p>";
											echo "</div>";
									echo "</div></a>";
								}
							echo "</div>";
						}
						else
						{
							if($filtro == "eventi")
								echo "<h4>Nessun Evento al Momento è Disponibile<br/>Prova con le <a class='link' href='eventi.php?filtro=visite'>Visite</a></h4>";
							elseif($filtro == "visite")
								echo "<h4>Nessuna Vista al Momento è Disponibile<br/>Prova con gli <a class='link' href='eventi.php?filtro=eventi'>Eventi</a></h4>";
						}
					}
					else
					{
						echo"<h3 class='tmp mt-5 mb-3'>Seleziona eventi o visite</h3>";
						echo"<a class='mr-1 btn btn-secondary mybtn transition-15s-easeio' role='button' href='eventi.php?filtro=eventi'>Eventi</a>";
						echo"<a class='ml-1 btn btn-secondary mybtn transition-15s-easeio' role='button' href='eventi.php?filtro=visite'>Visite</a>";
					}
				?>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
		</div>
	</body>
</html>
