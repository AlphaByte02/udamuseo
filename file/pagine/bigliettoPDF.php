<?php
	require_once "../struct/include/functions.php";

	if(!isset($_SESSION))
		session_start();

	if(isset($_SESSION["idBiglietti"]))
	{
		$idBiglietti = $_SESSION["idBiglietti"];

		$db = new DB();

		$infoBiglietti = array();
		$resBiglietti = $db->runQuery("SELECT b.id_biglietto AS 'idBiglietto', v.titolo AS 'titolo', c.descrizione AS 'categoria', v.data_inizio, v.data_fine FROM biglietti AS b JOIN visite AS v ON b.codice_visita=v.codice JOIN categorie AS c ON b.codice_categoria=c.codice");
		if($resBiglietti !== false && $resBiglietti->num_rows > 0)
		{
			while ($biglietto = $resBiglietti->fetch_assoc())
			{
				if(in_array($biglietto["idBiglietto"], $idBiglietti))
				{
					$infoBiglietti[$biglietto["idBiglietto"]] = array("titolo" => $biglietto["titolo"], "categoria" => $biglietto["categoria"], "data_inizio" => $biglietto["data_inizio"], "data_fine" => $biglietto["data_fine"]);
					$resAccesori = $db->runQuery("SELECT s.descrizione AS 'accessorio' FROM accessori AS a JOIN servizi AS s ON a.codice_servizio=s.codice WHERE a.id_biglietto='$biglietto[idBiglietto]'");
					if($resAccesori !== false && $resAccesori->num_rows > 0)
						while ($accessorio = $resAccesori->fetch_assoc())
							$infoBiglietti[$biglietto["idBiglietto"]]["accessori"][] = $accessorio["accessorio"];
				}
			}
		}

		$pdf = new FPDF();
		$pdf->SetAutoPageBreak(false);

		$contaBiglietti = 0;
		foreach ($infoBiglietti as $idBiglietto => $info)
		{
			if($contaBiglietti % 5 == 0 || $contaBiglietti == 0)
			{
				$pdf->AddPage();
				$pdf->SetFont('Arial', 'B', 16);
				$pdf->Image(_DOCUMENT_ROOT_ . "/file/struct/immagini/logoPDF.jpg", 10, 10, 40, 40);
				$pdf->SetXY(60, 10);
				$pdf->Cell(140, 10, "Museo Severi", 1, 0, "C");
				$pdf->SetXY(60, 30);
				$pdf->SetFont('Arial', 'B', 12);
				$pdf->MultiCell(140, 5, utf8_decode("Questo è il tuo biglietto per l'entrata al museo, stampalo e consegnalo alla cassa"), 0, "L", false);
				$x = 10;
				$y = 60;
			}

			$titolo = $info["titolo"];
			$categoria = $info["categoria"];

			$qrcode = new QRcode($idBiglietto, "H");
			$qrcode->displayFPDF($pdf, $x, $y, 40);

			$pdf->SetXY($x + 50, $y);

			$contaBiglietti += 1;
			$descrizione = "Biglietto N° $contaBiglietti\nBiglietto per: $titolo\nTipo biglietto: $categoria";
			if(!is_null($info["data_inizio"]) && !is_null($info["data_fine"]))
			{
				$data_inizio = new DateTime($info["data_inizio"]);
				$data_inizio = $data_inizio->format("d") . " " . getNameMonthIta($data_inizio->format("m")) . " " . $data_inizio->format("Y");
				$data_fine = new DateTime($info["data_fine"]);
				$data_fine = $data_fine->format("d") . " " . getNameMonthIta($data_fine->format("m")) . " " . $data_fine->format("Y");

				$descrizione .= "\nDate: $data_inizio al $data_fine";
			}
			else
				$descrizione .= "\nDate: Fino a Chiusura Visita";

			if(isset($info["accessori"]) && count($info["accessori"]) > 0)
				$descrizione .= "\nAccessori: " . implode(", ", $info["accessori"]);
			else
				$descrizione .= "\nAccessori: Nessun Accessorio Acquistato";

			$pdf->SetFont('Arial', 'B', 10);
			$pdf->MultiCell(140, 8, utf8_decode($descrizione), 1, "L", false);

			$y += 45;
			$pdf->SetXY($x,$y);
		}

		$db->closeConnection();
		$pdf->Output();
	}
?>
