<?php
	require_once "file/struct/include/functions.php";

	if(!isset($_SESSION))
	{
		session_start();
		session_regenerate_id();
	}
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>

		<!-- SEO -->
		<title>Severi Museo - Museo Storico del Severi a Padova</title>
		<meta name="description" content="Museo Storico del Severi situato a Padova, tutta la storia e le foto degli archivi storici">
		<meta property="og:title" content="Severi Museo - Museo Storico del Severi a Padova">
		<meta property="og:image" content="<?php echo _ROOT_DIR_ . '/file/struct/immagini/favicon.png';?>">
		<meta property="og:site_name" content="Severi Museo">

		<style media="screen">
			#mainImg {
				background-image: url('<?php echo _ROOT_DIR_ . "/file/struct/immagini/cover_homepage.jpg"; ?>');
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
			}

			.bottoneR:hover{
				background-color: #138496 !important;
			}

			.btnRounded {
				color:white;
				margin-bottom:15px;
				background-color:white;
				color:#212529;
				border-radius: 50px;
				padding-top:5px;
				padding-bottom:5px;
			}

			@media only screen and (max-width: 767px) {
				.row {
					margin-top: 5px;
					margin-bottom: 5px;
				}
				.col-md-6 {
					margin-bottom: 5px;
				}
			}
		</style>

	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<?php
				$pagina="index";
				include _DOCUMENT_ROOT_ . "/file/struct/include/navbar.php";

				$db = new DB();

				$res = $db->runQuery("SELECT codice,titolo,descrizione FROM visite WHERE data_fine >= DATE(now()) ORDER BY data_inizio LIMIT 1");
				if($res !== false && $res->num_rows > 0)
				{
					$ev = $res->fetch_assoc();
					$idEv = $ev["codice"];
					$nomeEv = $ev["titolo"];
					$descEv = $ev["descrizione"];
				}

				$res = $db->runQuery("SELECT codice,titolo,descrizione FROM visite WHERE data_inizio IS NULL AND data_fine IS NULL ORDER BY RAND() LIMIT 1");
				if($res !== false && $res->num_rows > 0)
				{
					$vis = $res->fetch_assoc();
					$idVis = $vis["codice"];
					$nomeVis = $vis["titolo"];
					$descVis = $vis["descrizione"];
				}

				$db->closeConnection();
			?>
			<main role="main" class="pt-2">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div id="mainImg" class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
								<div class="col-md-5 p-lg-5 mx-auto my-5">
									<h1 class="display-4 font-weight-normal" style="color:white; text-shadow: 2px 2px #000000;">Museo Storico del Severi</h1>
									<p class="lead font-weight-normal" style="color:white; text-shadow: 2px 2px 3px #000000;">Tutta la Storia e le Foto degli archivi Storici dell'ITI Severi a Padova.</p>
									<a class="mybtn-secondary transition-15s-easeio" href="<?php echo _ROOT_DIR_ . "/file/pagine/eventi.php" ?>" >Acquista ora</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<?php if (isset($idEv)): ?>
								<a href="file/pagine/biglietteria.php?id=<?php echo $idEv;?>" style="text-decoration:none;">
									<div class="my-auto bg-dark w-100 p-5 bottoneR transition-15s-easeio">
										<h3 class="btnRounded">Prossimo evento disponibile</h3>
										<h2 class="display-5"><?php echo $nomeEv;?></h2>
										<p class="lead"><?php echo $descEv;?></p>
									</div>
								</a>
							<?php else: ?>
								<div class="my-auto bg-dark w-100 p-5 bottoneR"><h3 class="btnRounded">Nessun Evento Disponibile</h3></div>
							<?php endif; ?>
						</div>
						<div class="col-md-6">
							<?php if (isset($idVis)): ?>
								<a href="<?php echo _ROOT_DIR_ . '/file/pagine/biglietteria.php?id=' . $idVis;?>" style="text-decoration:none;">
									<div class="my-auto bg-dark w-100 p-5 bottoneR transition-15s-easeio">
										<?php  ?>
										<h3 class="btnRounded">Prossima visita disponibile</h3>
										<h2 class="display-5"><?php echo $nomeVis;?></h2>
										<p class="lead"><?php echo $descVis;?></p>
									</div>
								</a>
							<?php else: ?>
								<div class="my-auto bg-dark w-100 p-5 bottoneR"><h3 class="btnRounded">Nessuna Visita Disponibile</h3></div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
		</div>
	</body>
</html>
