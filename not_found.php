<?php
	require_once "file/struct/include/functions.php";
?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<?php require _DOCUMENT_ROOT_ . "/file/struct/include/sharedHead.php"; ?>
		<title>404</title>
	</head>
	<body class="text-center">
		<div class="cover-container d-flex flex-column">
			<main role="main" class="pt-2">
				<h1>Errore 404: Pagina non trovata!</h1>
				<h3>Prova a tornare alla HomePage dalla barra di navigazione</h3>
			</main>
			<?php include _DOCUMENT_ROOT_ . "/file/struct/include/footer.php"; ?>
		</div>
	</body>
</html>
